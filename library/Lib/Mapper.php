<?php

/**
 * Базовый класс для всех распределителей данных.
 * Содержит минимальный набор необходимых операций
 *
 * @author Twilight
 */
abstract class Lib_Mapper
{

    protected $_dbTable;            // Объект таблицы БД
    protected $_dbTableStringName;  // Строковое имя класса для таблицы БД
    protected $_modelStringName;    // Строковое имя класса модели
    protected $_uniqueKey;          // Уникальный ключ

    /**
     * Назначение распределителю имени таблицы
     * @param $dbTable      - строка, имя класса таблицы
     * @return Lib_Mapper   - возвращает ссылку на свой экземпляр
     * @throws Exception    - если переданное имя не является именем класса таблицы
     */
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {

            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    /**
     * Получение сохраненного экземпляра объекта класса таблицы
     * @return mixed - объект класса-наследника Zend_Db_Table_Abstract
     */
    public function getDbTable()
    {
        if (null === $this->_dbTable) { // если еще не создан, создаем новый
            $this->setDbTable($this->_dbTableStringName);
        }
        return $this->_dbTable;
    }

    /**
     * Сохранение таблицы в БД
     * @param Lib_Model $model  - класс модели для сохранения
     * @return mixed            - возвращает false, либо индекс сохраненной записи
     */
    public function save(Lib_Model $model)
    {
        $data = $model->toArray();
        return $this->saveOrUpdateData($model, $data);
    }

    /**
     * Вспомогательная функция сохранения модели в БД
     * @param $model - модель для сохранения
     * @param $data  - ассоциативный массив параметров
     * @return mixed - false при неудаче, либо индекс записи
     */
    public function saveOrUpdateData($model, $data)
    {
        $key = $this->_uniqueKey;
        $id = $model->$key;
        if (is_null($id) || '' == $id) {
            // Убеждаемся что ключевого поля точно не будет
            unset($data[$key]);
            return $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array($key . ' = ?' => $id));
            return $id;
        }
    }

    /**
     * Поиск данных по индексу
     * @param $id       - целочисленный индекс
     * @return mixed    - модель с данными, либо null
     */
    public function find($id)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $model = new $this->_modelStringName();
        $model->setOptions($row->toArray());
        return $model;
    }

    /**
     * Получение всех записей в таблице в виде моделей
     * @return array - массив моделей
     */
    public function fetchAll()
    {
        $select = $this->getDbTable()->select();
        $resultSet = $this->getDbTable()->fetchAll();
        $entries = array();
        foreach ($resultSet as $row) { // массив создания моделей из объектов-строк
            $entry = new $this->_modelStringName();
            $entry->setOptions($row->toArray());
            $entries[] = $entry;
        }
        return $entries;
    }

    /**
     * Удаление записи по индексу
     * @param $id - целочисленный индекс
     */
    public function delete($id)
    {
        $key = $this->_uniqueKey;
        //TODO поменять на поиск+удаление (для удаления связанных)
        $this->getDbTable()->delete(array($key . ' =  ?' => (int)$id));
    }

}

?>
