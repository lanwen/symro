<?php

/**
 * Description of Form
 *
 * @author Twilight
 */
    class Lib_Form extends Zend_Form {
     /**
     * Инициализация формы
     * 
     * return void
     */  
    public function init()
    {
        // Вызов родительского метода
        parent::init();
        
        // Создаем объект переводчика, он нам необходим для перевода сообщений об ошибках.
        // В качестве адаптера используется php массив
        $translator = Zend_Validate::getDefaultTranslator();
        
        // Задаем объект переводчика для формы
        $this->setTranslator($translator);
        
        /* Задаем префиксы для самописных элементов, валидаторов, фильтров и декораторов.
           Благодаря этому Zend_Form будет знать где искать наши самописные элементы */
        $this->addElementPrefixPath('Lib_Validate', 'Lib/Validate/', 'validate');
        $this->addElementPrefixPath('Lib_Filter', 'Lib/Filter/', 'filter');
        $this->addElementPrefixPath('Lib_Form_Decorator', 'Lib/Form/Decorator/', 'decorator');
    }
}

?>
