<?php

/**
 * Базовый класс для всех моделей.
 * Содержит функции получения и сохранения данных в модели
 *
 * @author Twilight
 */
abstract class Lib_Model {

    /**
     * Конструктор
     * @param array|null $options - может принимать ассоциативный массив параметров для начальной инициализации
     */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * Создает динамический сеттер вида setСвойство(значение)
     * @param type $name - имя свойства
     * @param type $value - назначаемое значение
     * @throws Exception - если нужного сеттера не будет найдено
     */
    public function __set($name, $value) {
        $method = 'set' . ucfirst($name);
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid property: ' . $name);
        }
        $this->$method($value);
    }

    /**
     * Создает динамический геттер при обращении к свойству в виде getСвойство()
     * @param type $name - имя свойства
     * @return type mixed - обычно String, но могут быть исключения
     * @throws Exception - если не найдет нужного геттера
     */
    public function __get($name) {
        $method = 'get' . ucfirst($name); // Повышаем регистр первой буквы и добавляем к get
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid property: ' . $name);
        }
        return $this->$method();
    }

    /**
     * Для каждого ключа в массиве вызывает сеттер в модели
     * @param array $options - ассоциативный массив свойств
     * @return \Lib_Model - возвращает себя 
     */
    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key); // Повышаем регистр первой буквы и добавляем к set
            if (in_array($method, $methods)) {
                $this->$method($value); // Вызов сеттеров в цикле, если такие существуют
            }
        }
        return $this;
    }

    /**
     * Перебирает все геттеры,
     * свойства, которые не null добавляет в массив
     * @return array - ассоциативный массив свойств
     */
    public function toArray() {
        $array = array();

        $methods = get_class_methods($this);
        foreach ($methods as $method) {
            
            if (preg_match('/^get((?!Options).*)/', $method, $matches)) { // Нужны только геттеры
                $prop = lcfirst($matches[1]); // Понижаем регистр первой буквы
                $value = $this->$method(); // Вызываем геттер
                if (!is_null($value) && '' != $value) {
                // Если свойство определено и не пустое, то добавляем пару в массив
                    $array[$prop] = $value;
                }
            }
        }

        return $array;
    }



}

?>
