<?php


class Lib_Form_Decorator_Calendar extends Zend_Form_Decorator_Abstract
{

    
    /**
     * Получение кода ссылки и изображения каледаря. Настройка календаря
     *
     * @return string
     */
    private function _getCalendarLink()
    {

        $calendarLink = '
            <script>
	$(function() {
            
            $( "#'.$this->getElement()->getName().'" ).datepicker({
			changeMonth: true,
			changeYear: true,
                        regional: "ru",
                        dateFormat: "yy-mm-dd",
		});
         
		});

	</script>
            <div class="input-prepend">
              <span class="add-on"><i class="icon-calendar"></i></span>
            
               

        ';
        
        return $calendarLink;
    }
    
    
    /**
     * Рендеринг декоратора
     *
     * @param string $content
     * @return string
     */
    public function render($content)
    {
        // Получаем объект элемента к которому применяется декоратор
        $element = $this->getElement();
        if (!$element instanceof Zend_Form_Element) {
            return $content;
        }
        // Проверяем объект вида зарегистрированного для формы
        if (null === $element->getView()) {
            return $content;
        }
     return $this->_getCalendarLink().$content.'</div>';

    }
}