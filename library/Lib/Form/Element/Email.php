<?php

/**
 * App_Form_Element_Email
 * 
 * Элемент формы - электронная почта
 * 
 * @author Александр Махомет aka San для http://zendframework.ru
 */
class Lib_Form_Element_Email extends Zend_Form_Element_Text {
//<span class="add-on"><i class="icon-envelope"></i></span>
    /**
     * Инициализация элемента
     * 
     * return void
     */
    public function init() {
        $this->setLabel('Электронная почта:');
        $this->setAttrib('maxlength', 80);
        $this->addValidator('EmailAddress', true);
        $dbValidator = $validator = new Zend_Validate_Db_NoRecordExists(array(
                            'table' => 'users',
                            'field' => 'email'
                        ));
        $dbValidator->setMessages(array(Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => 'Почта %value% уже есть'));
        $this->addValidator($dbValidator);
        //  $this->addValidator('NoDbRecordExists', true, array('users', 'email'));
        $this->addFilter('StringTrim');
    }

}