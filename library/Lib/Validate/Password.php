<?php

/**
 * Lib_Validate_Password
 * 
 * Валидация пароля
 * 
 * 
 */
class Lib_Validate_Password extends Zend_Validate_Abstract 
{
    /**
     * Метка ошибки
     * @var const 
     */    
    const INVALID = 'passwordInvalid';

    /**
     * Метка ошибки
     * @var const 
     */    
    const INVALID_LENGTH = 'passwordBadLength';    
    
    /**
     * Текст ошибки
     * @var array 
     */
    protected $_messageTemplates = array(
        self::INVALID => 'Введенное значение не удовлетворяет требованиям пароля',
        self::INVALID_LENGTH => 'Пароль должен быть длиннее 2 и менее 15 символов'
    );

    /**
     * Проверка пароля
     * 
     * @param string $value значение которое поддается валидации
     */
    public function isValid($value) 
    {
        // Благодаря этому методу значение будет автоматически подставлено в текст ошибки при необходимости
        $this->_setValue($value);
        
        // Валидатор проверки длины
        $validatorStringLength = new Zend_Validate_StringLength(3, 15);
        
        // Проверка на допустимые символы
        if (!preg_match("/^[~!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\\\\/\\{\\}\\[\\].,\\?<>:;a-z0-9]*$/i", $value)) {
            // С помощью этого метода мы указываем какая именно ошибка произошла
            $this->_error(self::INVALID);
            return false;            
        }
        elseif (!$validatorStringLength->isValid($value)) {
            $this->_error(self::INVALID_LENGTH);
            return false;            
        }

        return true;
    }
}

