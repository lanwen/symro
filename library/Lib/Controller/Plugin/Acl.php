<?php

/**
 * Плагин контроля доступа
 * Проверяет, может ли текущий пользователь получить доступ к данной странице.
 * Если не может, перенаправляет на страницу ошибки
 *
 * @author Twilight
 */
class Lib_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{

    private $_acl = null; // Объект правил доступа

    /**
     * Конструктор, инициализирует поле объекта правил
     * @param Zend_Acl $acl - объект правил доступа
     */
    public function __construct(Zend_Acl $acl)
    {
        $this->_acl = $acl;
    }

    /**
     * Переопределение метода фреймворка
     * Метод вызывается перед отображением страницы пользователю и обработкой данных
     * @param Zend_Controller_Request_Abstract $request - объект запроса
     * @return bool - возвращает значение типа bool если дальнейшая проверка не требуется
     * @throws Zend_Application_Exception - в случае, если у пользователя нет разрешения на просмотр страницы
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        // Если контроллер ошибки, не проверяем права доступа
        if('error' == $request->getControllerName()) {
            return false;
        }

        // Получаем роль. По-умолчанию - гость.
        $role = (Zend_Auth::getInstance()->hasIdentity())
            ? Zend_Auth::getInstance()->getIdentity()->role
            : 'guest';

        //  Мы будем использовать контроллер в качестве ресурса.
        $resource = "mvc:" . $request->getControllerName();
        // И экшн в качестве привелегии
        $privelege = $request->getActionName();

        //TODO если юзер, то смотрим на юид и запрещаем, те что ниже - по ацл отсекутся

        if (!$this->_acl->isAllowed($role, $resource, $privelege)) {
            // Если недостаточно прав говорим об этом без обиняков
            throw new Zend_Application_Exception('Access denyed! Please check your permissions', 403);
        }

        // Особые проверки для агентского приложения
        if ('agent' == $role) {
            // Чтобы могли выйти без ввода ключа
            if('logout' == $request->getActionName()) {
                return false;
            }
            // Выдергиваем оид из логина
            // TODO доставать из таблички связи юзер-орг
            $oid = preg_replace("/\D*/", "",Zend_Auth::getInstance()->getIdentity()->login);
            $orgM = new Application_Model_OrganizationMapper();
            $org = $orgM->find($oid);

            // Если организация существует
            if (!is_null($org)) {
                $agent = Zend_Registry::get('agent');
                // Генерируем новый ключ
                $generatedHash = md5(md5($org->oid . $org->prefix . $agent['salt']) . $agent['salt']);
                // Солим полученный ключ
                $hashFromAgent = md5($request->getParam('key') . $agent['salt']);

                // Если предоставленный агентом ключ (засоленный)
                // не совпадает со сгенерированным
                if($generatedHash!=$hashFromAgent) {
                    throw new Zend_Application_Exception('Access denyed! Please check all request params', 403);
                }
            }
        }
    }

}

?>
