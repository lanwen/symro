<?php

/**
 * App_Controller_Plugin_FlashMessenger
 * 
 * Плагин, проверяет наличие сообщений о успешных результатах и в случае если они есть, создает именованый сегмент
 * для их вывода в макете
 * 
 * @author Александр Махомет aka San для http://zendframework.ru
 */
class Lib_Controller_Plugin_FlashMessenger extends Zend_Controller_Plugin_Abstract
{
    
   /**
    * Перехват события postDispatch
    * 
    */
    public function postDispatch()
    {
        
        //Zend_Debug::dump($request);
        // Инициализируем помощник FlashMessenger и получаем сообщения
        $actionHelperFlashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
       // $this->_helper->FlashMessenger->setNamespace('messages')->addMessage('Данные!');
        $messagesSuccess = $actionHelperFlashMessenger->setNamespace('messages')->getMessages();
        $messagesErrors = $actionHelperFlashMessenger->setNamespace('errors')->getMessages();
        //print_r($messagesSuccess);
        
        //Zend_Debug::dump($messagesSuccess);
        
        // Если сообщений нет, или процес диспетчеризации еще продолжается, просто выходим из плагина
        if (empty($messagesSuccess) && empty($messagesErrors) /*|| !$request->isDispatched()*/) {
            //echo 'no messages';
            return;
        }
       // echo 'Success!';
        // Получаем объект  Zend_Layout
        $layout = Zend_Layout::getMvcInstance();
        // Получаем объект  вида
        $view = $layout->getView();
        // Добавляем переменную для вида
        $view->messages = $messagesSuccess;
        $view->messagesErrors = $messagesErrors;

        // Устанавливаем объект вида с новыми переменным и производим рендеринг скрипта вида в сегмент messages
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setView($view)
                     ->renderScript('messages.phtml', 'messages');
       // exit();
    }
}