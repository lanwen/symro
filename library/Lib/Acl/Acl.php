<?php

/**
 * Description of Acl
 *
 * @author Twilight
 */
class Lib_Acl_Acl extends Zend_Acl {

    public function __construct() {
        //parent::__construct();
        $this->init();
    }

    protected function init() {

        // Создаем роли
        $this->addRole(new Zend_Acl_Role('guest'))
                ->addRole(new Zend_Acl_Role('agent'), 'guest')
                ->addRole(new Zend_Acl_Role('user'), 'guest')
                ->addRole(new Zend_Acl_Role('admin'), 'user')
                ->addRole(new Zend_Acl_Role('superadmin'), 'admin');
//TODO динамические роли доступа. Например user<UID> и доступ у него тоже динамически к ресурсам разрешен
        // Создаем ресурсы
        // Использую префиксы для наименования ресурсов
        // "mvc:" - для страниц
        //? "action:" - для действий
        //? Привилегия простого типа для вывода элементов управления: "simplepriv:"
        $this->addResource(new Zend_Acl_Resource('mvc:index'))
                ->addResource(new Zend_Acl_Resource('mvc:error'))
                ->addResource(new Zend_Acl_Resource('mvc:auth'))
                ->addResource(new Zend_Acl_Resource('mvc:users'))
                ->addResource(new Zend_Acl_Resource('mvc:search'))
                ->addResource(new Zend_Acl_Resource('mvc:msg'))
                ->addResource(new Zend_Acl_Resource('mvc:org'))
                ->addResource(new Zend_Acl_Resource('mvc:comp'));

        //Что не разрешено - запрещено
        $this->deny(null, null, null);
        // Пускаем гостей на "морду" и на страницу ошибок
        $this->allow('guest', array('mvc:error', 'mvc:index'));

        // А также на страницы авторизации
        $this->allow('guest', 'mvc:auth', array('login'));
        // А мемберам уже облом :)
        $this->deny('user', 'mvc:auth', array('login', 'register'));
        $this->deny('agent', 'mvc:auth', array('login', 'register'));
        // Выход.
        $this->allow('user', 'mvc:auth', array('logout'));
        $this->allow('agent', 'mvc:auth', array('logout'));
        // Админам разрешаем регистрировать
        $this->allow('admin', 'mvc:auth', array('register'));

        // На страницы просмотра и редактирования своего профиля
        $this->allow('user', 'mvc:users', array('profile', 'editself'));

        //Юзерам запрещено просматривать список организаций и компов других пользователей, админам - нет
        $this->deny('user', 'mvc:users', array('viewuserorg', 'viewusercomp'));
        $this->allow('admin', 'mvc:users', array('viewuserorg', 'viewusercomp'));

        // Админам просматривать всех, удалять юзеров, редактирования профиля, добавлять и снимать организации
        $this->allow('admin', 'mvc:users',
            array('index','del', 'delorg', 'addorg', 'editprofile', 'addcomp', 'delcomp'));

        $this->allow('user', 'mvc:comp' , array('index', 'view'));
        // Запрет юзерам видеть все компы и разрешение это делать админам
        $this->deny('user', 'mvc:comp' , array('viewall'));
        $this->allow('admin', 'mvc:comp' , array('viewall'));
        // Позволяем админам и агентам редактировать компы
        $this->allow('admin', 'mvc:comp' , array('add', 'del', 'edit'));
        $this->allow('agent', 'mvc:comp' , array('add', 'del', 'edit'));

        $this->allow('user', 'mvc:org' , array('index', 'view'));

        // Запрет юзерам видеть все организации и разрешение на это админам
        $this->deny('user', 'mvc:org' , array('viewall'));
        $this->allow('admin', 'mvc:org' , array('viewall'));
        // Позволяем админам редактировать организации, а так же инициализировать агента и конфиг
        $this->allow('admin', 'mvc:org' , array('add', 'del', 'edit', 'agentcreate', 'config'));


        // Юзеры могут смотреть свои заявки и отправлять их админам
        $this->allow('user', 'mvc:msg', array('send', 'outbox', 'read'));
        $this->deny('user', 'mvc:msg', array('sendnotadmins'));

        // Админы могут отправлять письма простым пользователям
        $this->allow('admin', 'mvc:msg', array('sendnotadmins'));

        // Админы так же могут смотреть входящие заявки и менять статус
        $this->allow('admin', 'mvc:msg', array('inbox', 'changestatus'));


        //$this->allow('user', 'mvc:search', array('search'));

        
        //! Пока разрешим гостям просмотр организаций и компов
       // $this->allow('guest', 'mvc:comp'/* , array('index') */);
       // $this->allow('guest', 'mvc:org'/* , array('index') */);
    }

}

?>
