<?php

class Application_Model_ComputersMapper extends Lib_Mapper
{

    public function __construct()
    {
        $this->_dbTableStringName = 'Application_Model_DbTable_Computers';
        $this->_modelStringName = 'Application_Model_Computers';
        $this->_uniqueKey = 'cid';
    }

    public function fetchAll($uid = null)
    {
        $select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
            ->setIntegrityCheck(false);

        $select->join(array('o' => 'organization'), 'computers.oid = o.oid', array('orgName' => 'name'));

        if(!is_null($uid)) {
            $select->join(array('uc' => 'usercomp'), 'computers.cid = uc.cid', array())
                ->where('uc.uid = ?', (int)$uid);
        }

        $resultSet = $this->getDbTable()->fetchAll($select);

        // Массив будущих компов
        $entries = array();

        foreach ($resultSet as $row) {
            $entry = new $this->_modelStringName();
            $entry->setOptions($row->toArray());
            $entries[] = $entry;
        }
        return $entries;
    }


    public function fetchByOid($oids)
    {
        $select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
            ->setIntegrityCheck(false);

        $select->join(array('o' => 'organization'), 'computers.oid = o.oid', array('orgName' => 'name'));

        if(is_array($oids)) {
            foreach($oids as $oid) {
                $select->orWhere('o.oid = ?', (int)$oid);
            }
        } else {
            $select->orWhere('o.oid = ?', (int)$oids);
        }

        $resultSet = $this->getDbTable()->fetchAll($select);
        // Массив будущих компов
        $entries = array();

        foreach ($resultSet as $row) {
            $entry = new $this->_modelStringName();
            $entry->setOptions($row->toArray());
            $entries[] = $entry;
        }
        return $entries;
    }

    public function find($id)
    {
        $select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
            ->setIntegrityCheck(false);
        $select->where('cid = ?', (int)$id)
            ->join(array('o' => 'organization'), 'computers.oid = o.oid', array('orgName' => 'name'));

        $result = $this->getDbTable()->fetchAll($select);
        if (0 == count($result)) {
            return;
        }

        $row = $result->current();

        $model = new $this->_modelStringName();
        //Zend_Debug::dump($model);
        //Zend_Debug::dump($row->toArray());
        $model->setOptions($row->toArray());
        return $model;
    }

    /**
     * Временно тут - потом надо отрефакторить для всех при необходимости
     * @param Application_Model_Computers $model
     * @return mixed
     */
    public function save(Application_Model_Computers $model)
    {

        $data = $model->toArray();

        // При сохранении нам нужно текущее время
        $data['lastupdate'] = new Zend_Db_Expr('NOW()');

        $key = $this->_uniqueKey;
        $id = $model->$key;
        if (is_null($id) || '' == $id) {
            // Убеждаемся что ключевого поля точно не будет
            unset($data[$key]);
            return $this->getDbTable()->insertOrUpdate($data, $data);
        } else {
            $this->getDbTable()->update($data, array($key . ' = ?' => $id));
            return $id;
        }
    }

    /**
     * Проверяет, есть ли запись о связи указанного компьютера и пользователя
     * @param $cid - ид компа
     * @param $uid - ид юзера
     * @return bool - true если запись есть
     */
    public function isCompOfUser($cid, $uid) {
        $this->setDbTable('Application_Model_DbTable_Usercomp');
        $select = $this->getDbTable()->select();
        $select->where('cid = ?', (int) $cid)
            ->where('uid = ?', (int)$uid);
        $resultSet = $this->getDbTable()->fetchAll($select);

        $this->setDbTable($this->_dbTableStringName);

        return 0<count($resultSet);
    }

}

