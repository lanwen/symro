<?php

class Application_Model_Users extends Lib_Model
{

    protected $_uid;
    protected $_login;
    protected $_password;
    protected $_firstname;
    protected $_lastname;
    protected $_telefon;
    protected $_info;
    protected $_role;
    protected $_email;
    protected $_oids = array(); //array oid=>orgName
    protected $_cids = array(); //array

    function __construct(array $options = null)
    {
        parent::__construct($options);
    }

    public function getOids()
    {
        return $this->_oids;
    }

    public function setOids(array $oids)
    {
        if (is_numeric(current($oids))) { // Если мы получили массив из формы (он индексный, не ассоциативный),
            // то сохраняем массив как oid=>oid
            // (чтобы при записи перебирать только ключи)
            $this->_oids = array_combine($oids, $oids);
        }
        else { // Иначе сохраняем как есть
            // FIXME: Есть вероятность появления артефактов. Нужно протестировать внимательно
            $this->_oids = $oids;
        }
        return $this;
    }

    /**
     * Добавление новых организаций существующему юзеру
     * @param $oid
     * @return Application_Model_Users|bool
     */
    public function addOid($oid) {
        if(!is_numeric($oid)) {
            return false;
        }
        if(array_key_exists($oid, $this->_oids)) {
            return $this;
        }
        $this->_oids[$oid] = $oid;
        return $this;
    }

    public function getUid()
    {
        return $this->_uid;
    }

    public function setUid($uid)
    {
        $this->_uid = $uid;
        return $this;
    }

    public function getLogin()
    {
        return $this->_login;
    }

    public function setLogin($login)
    {
        $this->_login = $login;
        return $this;
    }

    public function getPassword()
    {
        return $this->_password;
    }

    public function setPassword($password)
    {
        $this->_password = md5($password);
        return $this;
    }

    public function getFirstname()
    {
        return $this->_firstname;
    }

    public function setFirstname($firstname)
    {
        $this->_firstname = $firstname;
        return $this;
    }

    public function getLastname()
    {
        return $this->_lastname;
    }

    public function setLastname($lastname)
    {
        $this->_lastname = $lastname;
        return $this;
    }

    public function getTelefon()
    {
        return $this->_telefon;
    }

    public function setTelefon($telefon)
    {
        $this->_telefon = $telefon;
        return $this;
    }

    public function getInfo()
    {
        return $this->_info;
    }

    public function setInfo($info)
    {
        $this->_info = $info;
        return $this;
    }

    public function getRole()
    {
        return $this->_role;
    }

    public function setRole($role)
    {
        $this->_role = $role;
        return $this;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setEmail($email)
    {
        $this->_email = $email;
        return $this;
    }

    public function setCids($cids)
    {
        if (is_numeric(current($cids))) { // Если мы получили массив из формы (он индексный, не ассоциативный),
            // то сохраняем массив как oid=>oid
            // (чтобы при записи перебирать только ключи)
            $this->_cids = array_combine($cids, $cids);
        }
        else { // Иначе сохраняем как есть
            // FIXME: Есть вероятность появления артефактов. Нужно протестировать внимательно
            $this->_cids = $cids;
        }
        return $this;
    }

    public function getCids()
    {
        return $this->_cids;
    }

}

