<?php

class Application_Model_Msg extends Lib_Model
{
    protected $_mid;
    //protected $_suid;
    //protected $_ruid;
    protected $_datetime;
    protected $_subject;
    protected $_text;
    protected $_status;
    protected $_controldate;

    protected $_sender;
    protected $_recepient;

    const STATUS_NEW = 'new';
    const STATUS_OPEN = 'open';

    public static function arrayOfStatuses() {
        return array(
            0 => null,
            1 => self::STATUS_NEW,
            2 => self::STATUS_OPEN,
        );
    }

    public static function convertIntToStatus($statusNum) {
        $arrStatuses = self::arrayOfStatuses();
        if(!key_exists($statusNum, $arrStatuses)) {
            return null;
        }
        return $arrStatuses[$statusNum];
    }


    public function setControldate($controltime)
    {
        $this->_controldate = $this->_controldate = new Zend_Date($controltime, 'yyyy-MM-dd');
    }

    /**
     * @return mixed | Zend_Date
     */
    public function getControldate()
    {
        return $this->_controldate;
    }

    public function setDatetime($datetime)
    {
        $this->_datetime = $this->_datetime = new Zend_Date($datetime, 'yyyy-MM-dd HH:mm:ss');
    }

    /**
     * @return mixed | Zend_Date
     */
    public function getDatetime()
    {
        return $this->_datetime;
    }

    public function setMid($mid)
    {
        $this->_mid = $mid;
    }

    public function getMid()
    {
        return $this->_mid;
    }

    public function setRuid($ruid)
    {
        $this->getRecipient()->setUid($ruid);
    }

    public function getRuid()
    {
        return $this->getRecipient()->uid;
    }

    public function setStatus($status)
    {
        $this->_status = $status;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function setSubject($subject)
    {
        $this->_subject = $subject;
    }

    public function getSubject()
    {
        return $this->_subject;
    }

    public function setSuid($suid)
    {
        $this->getSender()->setUid($suid);
    }

    public function getSuid()
    {
        return $this->getSender()->uid;
    }

    public function setText($text)
    {
        $this->_text = $text;
    }

    public function getText()
    {
        return $this->_text;
    }

    public function setSender(Application_Model_Users $sender)
    {
        $this->_sender = $sender;
    }

    public function getSender()
    {
        if(is_null($this->_sender)) {
            $this->_sender = new Application_Model_Users();
        }
        return $this->_sender;
    }

    public function setRecipient(Application_Model_Users $recepient)
    {
        $this->_recepient = $recepient;
    }

    public function getRecipient()
    {
        if(is_null($this->_recepient)) {
            $this->_recepient = new Application_Model_Users();
        }
        return $this->_recepient;
    }


}

