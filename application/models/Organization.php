<?php

class Application_Model_Organization extends Lib_Model {

    protected $_oid;
    protected $_prefix;
    protected $_name;
    protected $_contacts;
    protected $_telefon;
    protected $_telefon2;

    public function __construct(array $options = null) {
        parent::__construct($options);
    }
    
    public function getOid() {
        return $this->_oid;
    }

    public function setOid($oid) {
        $this->_oid = (int) $oid;
        return $this;
    }

    public function getPrefix() {
        return $this->_prefix;
    }

    public function setPrefix($prefix) {
        $this->_prefix = (String) $prefix;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = (String) $name;
        return $this;
    }

    public function getContacts() {
        return $this->_contacts;
    }

    public function setContacts($contacts) {
        $this->_contacts = (String) $contacts;
        return $this;
    }

    public function getTelefon() {
        return $this->_telefon;
    }

    public function setTelefon($telefon) {
        $this->_telefon = (String) $telefon;
        return $this;
    }

    public function getTelefon2() {
        return $this->_telefon2;
    }

    public function setTelefon2($telefon2) {
        $this->_telefon2 = (String) $telefon2;
        return $this;
    }
}

