<?php

class Application_Model_Computers extends Lib_Model {

    
    protected $_cid;
    protected $_oid;
    protected $_name;
    protected $_os;
    protected $_drives;
    protected $_soft;
    protected $_services;
    protected $_ip;
    protected $_info;
    protected $_orgName;
    protected $_network;
    protected $_ram;
    protected $_cpu;
    protected $_harddrive;
    protected $_video;
    protected $_lastupdate;
    
    
    function __construct(array $options = null) {
        parent::__construct($options);
    }
    
    public function getOrgName() {
        return $this->_orgName;
    }

    public function setOrgName($orgName) {
        $this->_orgName = $orgName;
        return $this;
    }

        
    public function getCid() {
        return $this->_cid;
    }

    public function setCid($cid) {
        $this->_cid = $cid;
        return $this;
    }

    public function getOid() {
        return $this->_oid;
    }

    public function setOid($oid) {
        $this->_oid = $oid;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function getIp() {
        return $this->_ip;
    }

    public function setIp($ip) {
        $this->_ip = $ip;
        return $this;
    }

    public function getInfo() {
        return $this->_info;
    }

    public function setInfo($info) {
        $this->_info = $info;
        return $this;
    }

    public function setLastupdate($lastUpdate)
    {
        $this->_lastupdate = new Zend_Date($lastUpdate, 'yyyy-MM-dd HH:mm:ss');
    }

    /**
     * @return mixed | Zend_Date
     */
    public function getLastupdate()
    {
        return $this->_lastupdate;
    }

    public function setOs($os)
    {
        $this->_os = $os;
    }

    public function getOs()
    {
        return $this->_os;
    }

    public function setDrives($drives)
    {
        $this->_drives = $drives;
    }

    public function getDrives()
    {
        return $this->_drives;
    }

    public function setSoft($soft)
    {
        $this->_soft = $soft;
    }

    public function getSoft()
    {
        return $this->_soft;
    }

    public function setServices($services)
    {
        $this->_services = $services;
    }

    public function getServices()
    {
        return $this->_services;
    }

    public function setCpu($cpu)
    {
        $this->_cpu = $cpu;
    }

    public function getCpu()
    {
        return $this->_cpu;
    }

    public function setHarddrive($harddrive)
    {
        $this->_harddrive = $harddrive;
    }

    public function getHarddrive()
    {
        return $this->_harddrive;
    }

    public function setNetwork($network)
    {
        $this->_network = $network;
    }

    public function getNetwork()
    {
        return $this->_network;
    }

    public function setRam($ram)
    {
        $this->_ram = $ram;
    }

    public function getRam()
    {
        return $this->_ram;
    }

    public function setVideo($video)
    {
        $this->_video = $video;
    }

    public function getVideo()
    {
        return $this->_video;
    }


}

