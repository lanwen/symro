<?php

class Application_Model_UsersMapper extends Lib_Mapper
{
    public function __construct()
    {
        $this->_dbTableStringName = 'Application_Model_DbTable_Users';
        $this->_modelStringName = 'Application_Model_Users';
        $this->_uniqueKey = 'uid';
    }

    /**
     * Достаем юзера по логину. Логин уникальный параметр,
     * поэтому юзер либо один, либо null, если не нашли такого
     * @param string $login - логин пользователя
     * @return Application_Model_Users|null
     * @override Переопределяет метод предка
     */
    public function getUserByLogin($login)
    {
        $select = $this->getDbTable()->select();


        $select->where('login = ?', (String)$login);
        $result = $this->getDbTable()->fetchAll($select);

        if (0 == count($result)) {
            return null;
        }

        $row = $result->current();
        $model = new Application_Model_Users();
        $model->setOptions($row->toArray());


        $orgsArray = array();
        $orgs = $row->findDependentRowset('Application_Model_DbTable_Userorg');
        foreach ($orgs as $oidrow) {

            $orgsArray[$oidrow->oid] = $oidrow->findParentRow('Application_Model_DbTable_Organization')->name;
        }
        $model->setOids($orgsArray);
        return $model;
    }

    /**
     * Ищет юзера по ид. Автоматом добавляет в массив пары oid=>orgName из зависимых таблиц
     * [fix] - Если сохранять модель, включая выдернутый пароль - то пароль в бд перезаписывается
     * @override Переопределяет метод предка
     * @param $uid - ид пользователя
     * @return Application_Model_Users
     */
    public function find($uid)
    {

        $result = $this->getDbTable()->find($uid);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $model = new Application_Model_Users();
        $optArr = $row->toArray();
        unset($optArr['password']);
        $model->setOptions($optArr);


        $orgsArray = array();
        $orgs = $row->findDependentRowset('Application_Model_DbTable_Userorg');
        foreach ($orgs as $oidrow) {

            $orgsArray[$oidrow->oid] = $oidrow->findParentRow('Application_Model_DbTable_Organization')->name;
        }
        $model->setOids($orgsArray);


        $compArray = array();
        $comps = $row->findDependentRowset('Application_Model_DbTable_Usercomp');
        foreach ($comps as $comprow) {
            $compArray[$comprow->cid] = $comprow->findParentRow('Application_Model_DbTable_Computers')->name;
        }
        $model->setCids($compArray);
        return $model;
    }

    /**
     * Сохраняет в табличку юзеров пользователя.
     * Так же создает в табличке связи этого пользователя с выбранными организациями
     * @param Application_Model_Users  $model
     * @return mixed
     */
    public function save(Application_Model_Users $model)
    {

        $data = $model->toArray();
        $key = $this->_uniqueKey;
        $id = $model->$key;

        $orgArray = $model->getOids();
        unset($data['oids']); // FIXME: Когда ансетится массив целиком, его элементы тоже так делают?
        unset($data['cids']); // FIXME: Когда ансетится массив целиком, его элементы тоже так делают?

        if (is_null($id) || '' == $id) {
            // Убеждаемся что ключевого поля точно не будет
            unset($data[$key]);
            $insertid = $this->getDbTable()->insert($data);

            // Теперь добавляем связи юзер-организация
            $dataUserorg = array();
            foreach (array_keys($orgArray) as $oid) {
                $dataUserorg[] = array('uid' => (int)$insertid, 'oid' => (int)$oid);
            }
            // Меняем табличку
            $this->setDbTable('Application_Model_DbTable_Userorg');
            foreach ($dataUserorg as $userorgRow) {
                $this->getDbTable()->insert($userorgRow);
            }
            // Возвращаем обратно
            $this->setDbTable($this->_dbTableStringName);
            return $insertid;
        } else {
            $this->getDbTable()->update($data, array($key . ' = ?' => $id));
            return $id;
        }
    }


    public function deleteUserorg($uid, $oid)
    {
        $this->setDbTable('Application_Model_DbTable_Userorg');
        $this->getDbTable()->delete(array('uid =  ?' => (int)$uid, 'oid =  ?' => (int)$oid));
        $this->setDbTable($this->_dbTableStringName);
    }

    public function addUserorg($uid, $oid)
    {
        $this->setDbTable('Application_Model_DbTable_Userorg');
        $this->getDbTable()->insert(array('uid' => (int)$uid, 'oid' => (int)$oid));
        $this->setDbTable($this->_dbTableStringName);
    }

     public function deleteUsercomp($uid, $cid = null)
    {
        $where = array();
        if(is_null($cid)) {
            $where = array('uid =  ?' => (int)$uid);
        } else {
            $where = array('uid =  ?' => (int)$uid, 'cid =  ?' => (int)$cid);
        }
        $this->setDbTable('Application_Model_DbTable_Usercomp');
        $this->getDbTable()->delete($where);
        $this->setDbTable($this->_dbTableStringName);
    }

    public function addUsercomp($uid, $cid)
    {
        $this->setDbTable('Application_Model_DbTable_Usercomp');
        $this->getDbTable()->insert(array('uid' => (int)$uid, 'cid' => (int)$cid));
        $this->setDbTable($this->_dbTableStringName);
    }

    public function getUsersByOid($oid)
    {
        $this->setDbTable('Application_Model_DbTable_Userorg');
        $select = $this->getDbTable()->select();

        $select ->from($this->getDbTable(), array('uid'))
            -> where('oid = ?', (int)$oid)
           // ->columns(array())
        ;
        $rowset = $this->getDbTable()->fetchAll($select);
        $uids = array();
        foreach($rowset->toArray() as $uidArr) {
            $uids[] = $uidArr['uid'];
        }
        $this->setDbTable($this->_dbTableStringName);
        $rowset = $this->getDbTable()->find($uids);

        $entries = array();
        foreach ($rowset as $row) {
            $entry = new $this->_modelStringName();
            $entry->setOptions($row->toArray());
            if('superadmin' != $entry->role) {
                $entries[] = $entry;
            }
        }

        return $entries;
    }


    public function getUsersByCid($cid)
    {
        $this->setDbTable('Application_Model_DbTable_Usercomp');
        $select = $this->getDbTable()->select();

        $select ->from($this->getDbTable(), array('uid'))
            -> where('cid = ?', (int)$cid)
            // ->columns(array())
        ;
        $rowset = $this->getDbTable()->fetchAll($select);
        $uids = array();
        foreach($rowset->toArray() as $uidArr) {
            $uids[] = $uidArr['uid'];
        }
        $this->setDbTable($this->_dbTableStringName);
        $rowset = $this->getDbTable()->find($uids);

        $entries = array();
        foreach ($rowset as $row) {
            $entry = new $this->_modelStringName();
            $entry->setOptions($row->toArray());
            if('superadmin' != $entry->role) {
                $entries[] = $entry;
            }
        }

        return $entries;
    }


    public function fetchAll()
    {
        $select = $this->getDbTable()->select();
        $select
            ->where('NOT role = "agent"')
            ->where('NOT role = "superadmin"');
        if('admin' == Zend_Auth::getInstance()->getIdentity()->role) {
            $select->where('NOT role = "admin"');
        }

        $resultSet = $this->getDbTable()->fetchAll($select);
        $entries = array();
        foreach ($resultSet as $row) {
            $entry = new $this->_modelStringName();
            $entry->setOptions($row->toArray());
            $entries[] = $entry;
        }

        return $entries;
    }
}

