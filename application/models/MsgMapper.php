<?php

class Application_Model_MsgMapper extends Lib_Mapper
{
    public function __construct()
    {
        $this->_dbTableStringName = 'Application_Model_DbTable_Msg';
        $this->_modelStringName = 'Application_Model_Msg';
        $this->_uniqueKey = 'mid';
    }


    public function save(Application_Model_Msg $model)
    {

        $data = $model->toArray();
        unset($data['sender']);
        unset($data['recipient']);
        // При сохранении нам нужно текущее время
        $data['datetime'] = new Zend_Db_Expr('NOW()');
        $data['controldate'] = $model->getControldate()->toString('yyyy-MM-dd');

        $this->saveOrUpdateData($model, $data);
    }

    public function getMsgsByResipient($ruid, $status = null)
    {
        $select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART)->setIntegrityCheck(false);

        $select->where('ruid = ?', (int)$ruid)

        // ->columns(array())
        ->order('controldate')
            ->join(array('u' => 'users'),
            // Данные отправителя
            'suid = u.uid', array('firstname', 'lastname', 'login'));

        if(!is_null($status)) {
            $select->where('status = ?', $status); // FIXME: Надо ли как еще эскейпить статус?
        }

        $rowset = $this->getDbTable()->fetchAll($select);

        $entries = array();
        foreach ($rowset as $row) {
            $entry = new $this->_modelStringName();
            $rowAsArr = $row->toArray();
            // В модели таких свойств нет
            unset($rowAsArr['firstname']);
            unset($rowAsArr['lastname']);
            unset($rowAsArr['login']);
            $entry->setOptions($rowAsArr);

            $entry->sender->setOptions(
                array(
                    'firstname' => $row->firstname,
                    'lastname' => $row->lastname,
                    'login' => $row->login,
                ));

            $entries[] = $entry;
        }
        return $entries;
    }

public function getMsgsBySender($suid, $status = null)
    {
        $select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART)->setIntegrityCheck(false);

        $select->where('suid = ?', (int)$suid)
        // ->columns(array())
            ->order('controldate')
            ->join(array('u' => 'users'),
            // Данные получателя
            'ruid = u.uid', array('firstname', 'lastname', 'login'));

        if(!is_null($status)) {
            $select->where('status = ?', $status); // FIXME: Надо ли как еще эскейпить статус?
        }

        $rowset = $this->getDbTable()->fetchAll($select);

        $entries = array();
        foreach ($rowset as $row) {
            $entry = new $this->_modelStringName();
            $rowAsArr = $row->toArray();
            // В модели таких свойств нет
            unset($rowAsArr['firstname']);
            unset($rowAsArr['lastname']);
            unset($rowAsArr['login']);
            $entry->setOptions($rowAsArr);

            $entry->sender->setOptions(
                array(
                    'firstname' => $row->firstname,
                    'lastname' => $row->lastname,
                    'login' => $row->login,
                ));

            $entries[] = $entry;
        }
        return $entries;
    }


    public function find($mid) {
        $msg = parent::find($mid);
        $uid = Zend_Auth::getInstance()->getIdentity()->uid;
        // Если сообщение есть, но текущий юзер не принадлежит ни к отправителю, ни к получателю
        // возвращаем null

        if(is_null($msg) || !is_null($msg) && !($uid == $msg->ruid || $uid == $msg->suid)) {
            return null;
        }
        $usersMapper = new Application_Model_UsersMapper();
        $msg->setSender($usersMapper->find($msg->suid));;
        $msg->setRecipient($usersMapper->find($msg->ruid));;
        return $msg;
    }

}

