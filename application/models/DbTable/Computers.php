<?php

class Application_Model_DbTable_Computers extends Zend_Db_Table_Abstract
{

    protected $_name = 'computers';
    protected $_primary = 'cid';
    protected $_dependentTables = array(
        'Application_Model_DbTable_Usercomp',
    );


    /**
     * @param array $insertData
     * @param array $updateData
     * @return int
     * @throws Zend_Db_Adapter_Exception
     *
     *
     * USAGE:
     * $fooTable = new DbTable_Foo();
     * $fooTable->insertOrUpdate(array(
     * 'field_1' => 'value',
     * 'field_2' => new Zend_Db_Expr('NOW()'),
     * ), array(
     *  'field_3' => 'other value',
     *  'field_4' => new Zend_Db_Expr('NOW()'),
     * ));
     *
     */
    public function insertOrUpdate(array $insertData, array $updateData)
    {
        $db = $this->getAdapter();
        $table = ($this->_schema ? $this->_schema . '.' : '') . $this->_name;

        // extract and quote col names from the array keys
        $i = 0;
        $bind = array();
        $insert_cols = array();
        $insert_vals = array();
        $update_cols = array();
        $update_vals = array();
        foreach (array('insert', 'update') as $type) {
            $data = ${"{$type}Data"};
            $cols = array();
            $vals = array();
            foreach ($data as $col => $val) {
                $cols[] = $db->quoteIdentifier($col, true);
                if ($val instanceof Zend_Db_Expr) {
                    $vals[] = $val->__toString();
                } else {
                    if ($db->supportsParameters('positional')) {
                        $vals[] = '?';
                        $bind[] = $val;
                    } else {
                        if ($db->supportsParameters('named')) {
                            $bind[':col' . $i] = $val;
                            $vals[] = ':col' . $i;
                            $i++;
                        } else {
                            /** @see Zend_Db_Adapter_Exception */
                            throw new Zend_Db_Adapter_Exception(get_class($db) . " doesn't support positional or named binding");
                        }
                    }
                }
            }
            ${"{$type}_cols"} = $cols;
            unset($cols);
            ${"{$type}_vals"} = $vals;
            unset($vals);
        }

        // build the statement
        $set = array();
        foreach ($update_cols as $i => $col) {
            $set[] = sprintf('%s = %s', $col, $update_vals[$i]);
        }

        $sql = sprintf(
            'INSERT INTO %s (%s) VALUES (%s) ON DUPLICATE KEY UPDATE %s;',
            $db->quoteIdentifier($table, true),
            implode(', ', $insert_cols),
            implode(', ', $insert_vals),
            implode(', ', $set)
        );



        // execute the statement and return the lastInsertId or 0 on update
        if ($db->supportsParameters('positional')) {
            $bind = array_values($bind);
        }
        // Выполнение
        $stmt = $db->query($sql, $bind);

        //DEBUG
        //file_put_contents('E:\workspace\symro\public\file123.txt', $sql);

        // Если нужно будет знать количество подправленных строк
        // $result = $stmt->rowCount();


        return $db->lastInsertId();
    }


}

