<?php

class Application_Model_DbTable_Userorg extends Zend_Db_Table_Abstract
{

    protected $_name = 'userorg';
    protected $_primary = array('oid', 'uid');

    protected $_referenceMap = array(

        'User' => array(
            'columns' => 'uid',
            'refTableClass' => 'Application_Model_DbTable_Users',
        ),
        'Org' => array(
            'columns' => 'oid',
            'refTableClass' => 'Application_Model_DbTable_Organization',
        ),
    );


    /**
     * Должно сформировать инсерт-строку вставки сразу пачки значений
     * Ввиду ограниченного времени до релиза и малого количества вставок (думаю меньше десятка за раз)
     * Можно воспользоваться поэлементным стандартном инсертом от зенда
     * Потом можно будет потестировать написав мультивставку и решить нужна ли она вообще
     *
     * @param array $arrayOfAssocArrays
     * @return string
     */
    public function getMultiInsert(array $arrayOfAssocArrays) {
        // Нужно получить что то вроде INSERT INTO userorg (oid, uid) VALUES (?, ?), (?, ?)...
        $sqlString = 'INSERT INTO ' . $this->_name . ' (';
        foreach(array_keys(current($arrayOfAssocArrays)) as $key) {
          $sqlString .= $key . ', ';
        }
        $sqlString = substr($sqlString, 0, -2); // delete last ", "
        $sqlString .= ') VALUES ';
        for ($i = 0; $i < sizeof($arrayOfAssocArrays); $i++) {
            $sqlString .= '(';
            foreach(array_keys(current($arrayOfAssocArrays)) as $key) {
                $sqlString .= '? , ';
            }
            $sqlString = substr($sqlString, 0, -2); // delete last ", "
            $sqlString .= '), ';
        }
        $sqlString = substr($sqlString, 0, -2); // delete last ", "
        return $sqlString;
    }
}

