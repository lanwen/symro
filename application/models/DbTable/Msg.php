<?php

class Application_Model_DbTable_Msg extends Zend_Db_Table_Abstract
{

    protected $_name = 'msg';
    protected $_primary = 'mid';
    protected $_referenceMap = array(

        'Sender' => array(
            'columns' => 'suid',
            'refTableClass' => 'Application_Model_DbTable_Users',
        ),
        'Recepient' => array(
            'columns' => 'ruid',
            'refTableClass' => 'Application_Model_DbTable_Users',
        ),
    );


}

