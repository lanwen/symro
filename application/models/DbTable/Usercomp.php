<?php

class Application_Model_DbTable_Usercomp extends Zend_Db_Table_Abstract
{

    protected $_name = 'usercomp';
    protected $_primary = array('uid', 'cid');
    protected $_referenceMap = array(

        'User' => array(
            'columns' => 'uid',
            'refTableClass' => 'Application_Model_DbTable_Users',
        ),
        'Comp' => array(
            'columns' => 'cid',
            'refTableClass' => 'Application_Model_DbTable_Computers',
        ),
    );


}

