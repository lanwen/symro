<?php

class Application_Model_DbTable_Users extends Zend_Db_Table_Abstract
{

    protected $_name = 'users';
    protected $_primary = 'uid';
    protected $_dependentTables = array(
        'Application_Model_DbTable_Userorg',
        'Application_Model_DbTable_Msg',
        'Application_Model_DbTable_Usercomp',
    );

}

