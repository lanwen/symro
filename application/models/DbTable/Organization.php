<?php

class Application_Model_DbTable_Organization extends Zend_Db_Table_Abstract
{

    protected $_name = 'organization';
    protected $_primary = 'oid';
    protected $_dependentTables = array('Application_Model_DbTable_Userorg');


}

