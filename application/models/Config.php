<?php

class Application_Model_Config extends Lib_Model
{
    private $_baseurl;
    private $_oid;
    private $_login;
    private $_pwd;
    private $_key;

    public function setBaseurl($baseurl)
    {
        $this->_baseurl = $baseurl;
        return $this;
    }

    public function getBaseurl()
    {
        return $this->_baseurl;
    }

    public function setKey($key)
    {
        $this->_key = $key;
        return $this;
    }

    public function getKey()
    {
        return $this->_key;
    }

    public function setLogin($login)
    {
        $this->_login = $login;
        return $this;
    }

    public function getLogin()
    {
        return $this->_login;
    }

    public function setOid($oid)
    {
        $this->_oid = $oid;
        return $this;
    }

    public function getOid()
    {
        return $this->_oid;
    }

    public function setPwd($pwd)
    {
        $this->_pwd = $pwd;
        return $this;
    }

    public function getPwd()
    {
        return $this->_pwd;
    }
}

