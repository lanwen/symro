<?php

class Application_Model_OrganizationMapper extends Lib_Mapper
{

    public function __construct()
    {
        $this->_dbTableStringName = 'Application_Model_DbTable_Organization';
        $this->_modelStringName = 'Application_Model_Organization';
        $this->_uniqueKey = 'oid';
    }

    public function updatePrefix(Application_Model_Organization $org)
    {
        $data = array();
        if (!is_null($org->getOid())) {
            $data['prefix'] = (String)$org->getPrefix();
            $this->getDbTable()->update($data, array('oid = ?' => $org->getOid()));
        }
    }

    public function findByPrefix($prefix)
    {

        $select = $this->getDbTable()->select();


        $select->where('prefix = ?', (String)$prefix);
        $result = $this->getDbTable()->fetchAll($select);

        if (0 == count($result)) {
            return;
        }

        $row = $result->current();
        $model = new Application_Model_Organization();
        $model->setOptions($row->toArray());
        return $model;
    }

    public function fetchAllNotUserIn($uid)
    {
        $select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
            ->setIntegrityCheck(false);

        $select->where('NOT EXISTS (SELECT * FROM userorg uo WHERE uo.uid= ? AND uo.oid=organization.oid)', (int)$uid)
            ->group('organization.oid')
            ->joinLeft(array('o' => 'userorg'), 'organization.oid = o.oid', array('uoid' => 'oid'));
        $resultSet = $this->getDbTable()->fetchAll($select);
        $entries = array();
        foreach ($resultSet as $row) {
            $entry = new $this->_modelStringName();
            $entry->setOptions($row->toArray());
            $entries[] = $entry;
        }
        return $entries;
    }

    public function save(Application_Model_Organization $model)
    {
        $oid = parent::save($model);
        $userMapper = new Application_Model_UsersMapper();
        $userMapper->addUserorg(Zend_Auth::getInstance()->getIdentity()->uid, $oid);
        return $oid;
    }

    public function fetchAll($uid = null)
    {

        if(is_null($uid)) {
            return parent::fetchAll();
        }

        $select = $this->getDbTable()->select(Zend_Db_Table::SELECT_WITH_FROM_PART)->setIntegrityCheck(false);
        $select->join(array('o' => 'userorg'), 'organization.oid = o.oid', array())
        ->where('o.uid = ?', (int)$uid);

        $resultSet = $this->getDbTable()->fetchAll($select);
        $entries = array();
        foreach ($resultSet as $row) {
            $entry = new $this->_modelStringName();
            $entry->setOptions($row->toArray());
            $entries[] = $entry;
        }

        return $entries;
    }


    /**
     * Проверяет есть ли связть юзер-организация
     * @param $oid - ид организации
     * @param $uid - ид юзера
     * @return bool - true, если связь есть
     */
    public function isOrgOfUser($oid, $uid) {
        $this->setDbTable('Application_Model_DbTable_Userorg');
        $select = $this->getDbTable()->select();
        $select->where('oid = ?', (int) $oid)
            ->where('uid = ?', (int)$uid);
        $resultSet = $this->getDbTable()->fetchAll($select);

        $this->setDbTable($this->_dbTableStringName);

        return 0<count($resultSet);
    }

}

