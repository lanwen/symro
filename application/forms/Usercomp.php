<?php

class Application_Form_Usercomp extends Lib_Form
{

    /**
     * Инициализация
     */
    public function init() {
        parent::init();

        // Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction($helperUrl->url(array('uid' => $this->getAttrib('uid')), 'ucadd'));

        // Метод формы
        $this->setMethod('post');

        // Атрибут class для формы
        $this->setAttrib('class', 'form form-horizontal');

        $table = new Application_Model_ComputersMapper();
        $compsArr = $table->fetchByOid(array_keys($this->getAttrib('oids')));
        // Организация
        $cmp = new Zend_Form_Element_Multiselect('cids');
        $cmp->setLabel('Компьютеры')
            ->setRequired(true)
            ->setValue(array_keys($this->getAttrib('cids')))
            ->setAttrib('size', 20);

        foreach ($compsArr as $comp) {
            $cmp->addMultiOptions(array($comp->cid => $comp->oid.':'.$comp->name));
        }
        $this->addElement($cmp);

        // Кнопка Submit
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label' => 'Сохранить',
            'class' => 'btn btn-primary',
        ));

        $submit->setDecorators(array('ViewHelper'));

        $this->addElement($submit);



        // Общая информация
        $this->addDisplayGroup(
            array('cids'), 'orgGroup', array(
                'legend' => 'Добавить компьютер'
            )
        );


        // Группа полей кнопок
        $this->addDisplayGroup(
            array('submit'), 'buttonsGroup', array('legend' => '')
        );
    }

}

