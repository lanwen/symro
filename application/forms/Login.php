<?php

class Application_Form_Login extends Lib_Form
{

    public function init()
    {
        parent::init();
        
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction($helperUrl->url(array(), 'login'));

        // Метод формы
        $this->setMethod('post');

        // Атрибут class для формы
        $this->setAttrib('class', 'form-horizontal');
        

        // Login 
        $login = new Zend_Form_Element_Text('login', array(
                    'required' => true,
                    'label' => 'Логин',
                    'maxlength' => '15',
                    'filters' => array('StringTrim'),
                ));
        $this->addElement($login);

        // Pwd элемент пароль
        $password = new Zend_Form_Element_Password('password', array(
                    'required' => true,
                    'label' => 'Пароль',
                    'maxlength' => '15',
                    'value' => '',
                ));
        $this->addElement($password);

        $submit = new Zend_Form_Element_Submit('submit', array(
            'label' => 'Вход',
            'class' => 'btn-primary',
        ));
        $this->addElement($submit);
    }


}

