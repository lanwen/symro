<?php

class Application_Form_Editprofile extends Lib_Form
{

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();

        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction($helperUrl->url(array('uid' => $this->getAttrib('uid'),
            'controller' => 'users',
            'action' => 'editprofile'), 'default'));

        // Main subform
        $formEd = new Application_Form_Register();
        $formEd->setAction('');
        $formEd->removeElement('password');
        $formEd->removeElement('login');
        $formEd->removeElement('email');
        $formEd->removeElement('password_approve');
        $formEd->removeElement('oids');

        $formEd->removeDisplayGroup('authDataGroup');
        $formEd->removeDisplayGroup('orgGroup');

        $formEd->getElement('submit')->setLabel('Обновить');
        $this->addSubForm($formEd, 'editform');

        // Email subform
        $this->addSubForm(new Zend_Form(), 'emailchangeform');
        $this->emailchangeform->addElement(new Lib_Form_Element_Email('email', array()));
        $this->emailchangeform->addElement(new Zend_Form_Element_Submit('submitemail', array(
            'label' => 'Обновить e-mail',
            'class' => 'btn btn-primary',
        )));

        // PASSWORD subform
        $this->addSubForm(new Zend_Form(), 'pwdchangeform');
        // Pwd элемент пароль. Значение проверяется собственным валидатором пароля
        $password = new Zend_Form_Element_Password('password', array(
            'required' => false,
            'label' => 'Пароль',
            'maxlength' => '15',
            'validators' => array('Password'),
            'value' => '',
        ));
        $this->pwdchangeform->addElement($password);


        // Подтверждение пароля
        $passwordApprove = new Zend_Form_Element_Password('password_approve', array(
            'required' => false,
            'label' => 'Подтвердите пароль',
            'maxlength' => '15',
            'ignore' => true,
            'validators' => array(array('EqualInputs', true, array('password'))),
        ));
        $this->pwdchangeform->addElement($passwordApprove);
        $this->pwdchangeform->addElement(new Zend_Form_Element_Submit('submitpwd', array(
            'label' => 'Обновить пароль',
            'class' => 'btn btn-primary',
        )));

        $acl = new Lib_Acl_Acl();
        if ($acl->isAllowed(Zend_Auth::getInstance()->getIdentity()->role, 'mvc:users', 'editprofile')) {
            // Role subform
            $this->addSubForm(new Zend_Form(), 'rolechangeform');
            // Роли
            $role = new Zend_Form_Element_Select('role');
            $role->setLabel('Роль')
                ->setRequired(false);
            $role->addMultiOptions(array(
                'user' => 'Пользователь',
                'admin' => 'Администратор',
            ));
            $this->rolechangeform->addElement($role);
            $this->rolechangeform->addElement(new Zend_Form_Element_Submit('submitrole', array(
                'label' => 'Обновить статус',
                'class' => 'btn btn-primary',
            )));
        }

    }

}

