<?php

class Application_Form_AddOrg extends Lib_Form {

    /**
     * Инициализация 
     */
    public function init() {
        parent::init();

        // Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction($helperUrl->url(array('uid' => $this->getAttrib('uid')), 'uoadd'));

        // Метод формы
        $this->setMethod('post');

        // Атрибут class для формы
        $this->setAttrib('class', 'form form-horizontal');

        // Организация
        $org = new Zend_Form_Element_Multiselect('oids');
        $org->setLabel('Организация')
            ->setRequired(true);

        //TODO Фильтрация вывода только организаций, в каких есть сам админ
        $table = new Application_Model_OrganizationMapper();
        foreach ($table->fetchAllNotUserIn($this->getAttrib('uid')) as $organization) {
            $org->addMultiOptions(array($organization->oid => $organization->oid.':'.$organization->name));
        }
        $this->addElement($org);

        // Кнопка Submit
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label' => 'Сохранить',
            'class' => 'btn btn-primary',
        ));

        $submit->setDecorators(array('ViewHelper'));

        $this->addElement($submit);



        // Общая информация
        $this->addDisplayGroup(
                array('oids'), 'orgGroup', array(
            'legend' => 'Добавить организацию'
                )
        );


        // Группа полей кнопок
        $this->addDisplayGroup(
                array('submit'), 'buttonsGroup', array('legend' => '')
        );
    }

}

