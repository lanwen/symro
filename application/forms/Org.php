<?php

class Application_Form_Org extends Lib_Form
{

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();

        // Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction($helperUrl->url(array('controller' => 'org', 'action' => 'edit'), 'default'));

        // Метод формы
        $this->setMethod('post');

        // Атрибут class для формы
        $this->setAttrib('class', 'form form-horizontal');


        // NAME 
        $name = new Zend_Form_Element_Text('name', array(
            'required' => true,
            'label' => 'Имя организации',
            'maxlength' => '128',
            'validators' => array(
                array('StringLength', true, array(0, 128))),
            'filters' => array('StringTrim'),
        ));
        $this->addElement($name);

        // При создании нужно обязательно указывать префикс
        // Prefix
        $prefix = new Zend_Form_Element_Text('prefix', array(
            'required' => false,
            'label' => 'Опознавательный префикс',
            'maxlength' => '32',
            'validators' => array(
                array('Alnum', true, array(true)),
                array('StringLength', true, array(0, 32))),
            'filters' => array('StringTrim'),
        ));
        $this->addElement($prefix);

        // CONTACTS
        $contacts = new Zend_Form_Element_Textarea('contacts', array(
            'required' => false,
            'label' => 'Контактная информация',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '600',
            'filters' => array('StringTrim', 'HtmlEntities'),
        ));
        $this->addElement($contacts);


        // telefon
        $telefon = new Zend_Form_Element_Text('telefon', array(
            'required' => true,
            'label' => 'Телефон',
            'maxlength' => '15',
            'validators' => array(
                array('Alnum', true, array(true)), //TODO валидатор телефонного номера
                array('StringLength', true, array(0, 15))),
            'filters' => array('StringTrim'),
        ));
        $this->addElement($telefon);


        // telefon2
        $telefon2 = new Zend_Form_Element_Text('telefon2', array(
            'required' => false,
            'label' => 'Доп. телефон',
            'maxlength' => '15',
            'validators' => array(
                array('Alnum', true, array(true)), //TODO валидатор телефонного номера
                array('StringLength', true, array(0, 15))),
            'filters' => array('StringTrim'),
        ));
        $this->addElement($telefon2);


        $oid = new Zend_Form_Element_Hidden('oid', array(
            'required' => false,
            'value' => '',
        ));
        $oid->setDecorators(array('ViewHelper'));
        $this->addElement($oid);


        // Кнопка Submit
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label' => 'Сохранить',
            'class' => 'btn btn-primary',
        ));

        $submit->setDecorators(array('ViewHelper'));

        $this->addElement($submit);

        // Кнопка Reset, возвращает форму в начальное состояние
        $reset = new Zend_Form_Element_Reset('reset', array(
            'label' => 'Очистить',
            'class' => 'btn',
        ));

        // Перезаписываем декораторы, что-бы выставить две кнопки в ряд
        $reset->setDecorators(array('ViewHelper'));
        $this->addElement($reset);

        // Группируем элементы
        $this->addDisplayGroup(
            array('name', 'prefix'), 'nameGroup', array(
                'legend' => 'Имя организации'
            )
        );
        // Группируем элементы
        $this->addDisplayGroup(
            array('contacts', 'telefon', 'telefon2'), 'infoGroup', array(
                'legend' => 'Общая информация'
            )
        );

        // Группа полей связанных с личной информацией
       /* $this->addDisplayGroup(
            array('orgName'), 'orginfo', array(
                'legend' => 'Информация об организации'
            )
        );*/
        /* $this->addDisplayGroup(
          array('challenge'), 'captchaDataGroup', array(
          'legend' => 'Антиробот'
          )
          ); */

        // Группа полей кнопок
        $this->addDisplayGroup(
            array('agreeRules', 'submit', 'reset'), 'buttonsGroup', array('legend' => '')
        );
    }

}

