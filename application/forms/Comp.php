<?php

class Application_Form_Comp extends Lib_Form
{

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();

        // Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction($helperUrl->url(array('controller' => 'comp', 'action' => 'edit'), 'default'));

        // Метод формы
        $this->setMethod('post');

        // Атрибут class для формы
        $this->setAttrib('class', 'form-horizontal');


        // NAME 
        $name = new Zend_Form_Element_Text('name', array(
            'required' => true,
            'label' => 'Имя компьютера',
            'maxlength' => '32',
            'validators' => array(
               array('StringLength', true, array(1, 32))),
            'filters' => array('StringTrim'),
        ));
        $this->addElement($name);

        // IP
        $ip = new Zend_Form_Element_Text('ip', array(
            'required' => false,
            'label' => 'IP адрес',
            'maxlength' => '15',
            'value' => '192.168.1.1',
        ));
        $this->addElement($ip);

        // OS
        $os = new Zend_Form_Element_Textarea('os', array(
            'required' => false,
            'label' => 'OS info',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '800',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($os);

        // drives
        $drives = new Zend_Form_Element_Textarea('drives', array(
            'required' => false,
            'label' => 'Drive Info',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '800',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($drives);

        // soft
        $soft = new Zend_Form_Element_Textarea('soft', array(
            'required' => false,
            'label' => 'Soft Info',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '6000',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($soft);

        // services
        $services = new Zend_Form_Element_Textarea('services', array(
            'required' => false,
            'label' => 'Services Info',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '8000',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($services);


        // video
        $video = new Zend_Form_Element_Textarea('video', array(
            'required' => false,
            'label' => 'Video Info',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '800',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($video);


        // ram
        $ram = new Zend_Form_Element_Textarea('ram', array(
            'required' => false,
            'label' => 'RAM Info',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '800',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($ram);


        // cpu
        $cpu = new Zend_Form_Element_Textarea('cpu', array(
            'required' => false,
            'label' => 'CPU Info',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '800',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($cpu);


        // network
        $network = new Zend_Form_Element_Textarea('network', array(
            'required' => false,
            'label' => 'Network Info',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '800',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($network);

        // harddrive
        $harddrive = new Zend_Form_Element_Textarea('harddrive', array(
            'required' => false,
            'label' => 'Harddrive Info',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '800',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($harddrive);

        // INFO
        $info = new Zend_Form_Element_Textarea('info', array(
            'required' => false,
            'label' => 'Дополнительная информация',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '800',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($info);

        // Организация
        $org = new Zend_Form_Element_Select('orgName');

        $org->setLabel('Организация')
            ->setRequired(true);
        //TODO автоматическое выставление верной организации
        $table = new Application_Model_OrganizationMapper();
        foreach ($table->fetchAll() as $organization) {
            $org->addMultiOption($organization->oid, $organization->oid . ':' . $organization->name);
        }

        $this->addElement($org);


        //TODO Проверка неодинаковости ип, имени в одной организации

        $cid = new Zend_Form_Element_Hidden('cid', array(
            'required' => false,
            'value' => '',
        ));
        $cid->setDecorators(array('ViewHelper'));
        $this->addElement($cid);


        // Кнопка Submit
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label' => 'Сохранить',
            'class' => 'btn btn-primary',
        ));

        $submit->setDecorators(array('ViewHelper'));

        $this->addElement($submit);

        // Группируем элементы
        $this->addDisplayGroup(
            array('name', 'ip', 'info'), 'infoGroup', array(
                'legend' => 'Общая информация'
            )
        );

        // Группа полей связанных с различной информацией
        $this->addDisplayGroup(
            array('orgName'), 'orginfo', array(
                'legend' => 'Информация об организации'
            )
        );

        // Группа полей связанных с компьютерной инфо
        $this->addDisplayGroup(
            array('network', 'os', 'drives', 'harddrive', 'soft', 'video', 'services', 'soft', 'cpu', 'ram'),
            'difcompinfo', array(
                'legend' => 'Различная информация'
            )
        );
        /* $this->addDisplayGroup(
          array('challenge'), 'captchaDataGroup', array(
          'legend' => 'Антиробот'
          )
          ); */

        // Группа полей кнопок
        $this->addDisplayGroup(
            array('submit'), 'buttonsGroup', array('legend' => '')
        );
    }

}

