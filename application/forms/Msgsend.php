<?php

class Application_Form_Msgsend extends Lib_Form
{
    
    public function init()
    {
        parent::init();

        // Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction($helperUrl->url(array('ruid' => $this->getAttrib('ruid')), 'sendmsg'));

        // Метод формы
        $this->setMethod('post');

        // Атрибут class для формы
        $this->setAttrib('class', 'form form-horizontal');
        
        $subject = new Zend_Form_Element_Text('subject', array(
                    'required' => true,
                    'label' => 'Тема: ',
                    'maxlength' => '255',
                    'size' => '60',
                    'filters' => array('StringTrim','HtmlEntities'),
                ));
        $this->addElement($subject);
        
        
        $text = new Zend_Form_Element_Textarea('text', array(
                    'required' => true,
                    'label' => 'Текст сообщения: ',
                    'cols' => '50',
                    'rows' => '15',
                    'maxlength' => '800',
                    'filters' => array('HtmlEntities'),
            ));
        $this->addElement($text);

        // Дата рождения, исп нестандартный декоратор
        $controldate = new Zend_Form_Element_Text('controldate', array(
            'label' => 'Контрольная дата:',
            'maxlength' => '10',
            'validators' => array(array('Date', true, array('yyyy-MM-dd'))),
            'filters' => array('StringTrim'),
        ));
        // Удаляем существующие декораторы
        $controldate->clearDecorators();

        //Назначаем новые, включая свой
        $controldate->addDecorator('ViewHelper')
            ->addDecorator('Calendar')
            ->addDecorator('Errors')
            ->addDecorator('HtmlTag', array('tag' => 'dd'))
            ->addDecorator('label', array('tag' => 'dt'));

        $this->addElement($controldate);
        
        
        $submit = new Zend_Form_Element_Submit('submit', array(
                    'label' => 'Отправить',
                    'class' => 'btn btn-primary',
                ));

        $submit->setDecorators(array('ViewHelper'));

        $this->addElement($submit);

        $this->addDisplayGroup(
            array('subject', 'text', 'controldate'), 'orgGroup', array(
                'legend' => ''
            )
        );

        $this->addDisplayGroup(
            array('submit'), 'submitGroup', array(
                'legend' => ''
            )
        );


    }


}

