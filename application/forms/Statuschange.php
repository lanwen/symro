<?php

class Application_Form_Statuschange extends Lib_Form
{

    public function init()
    {
        parent::init();

        // Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction($helperUrl->url(array('controller' => 'msg',
            'action' => 'changestatus',
            'mid' => $this->getAttrib('mid')), 'default'));

        // Метод формы
        $this->setMethod('post');

        // Атрибут class для формы
        $this->setAttrib('class', 'form form-horizontal');

        // Статус
        $status = new Zend_Form_Element_Select('status');
        $status->setLabel('Статус')
            ->setRequired(false);
        foreach (Application_Model_Msg::arrayOfStatuses() as $key => $statusname) {
            $status->addMultiOptions(array($key => $statusname));
        }
        $this->addElement($status);

        // Кнопка Submit
        $submit = new Zend_Form_Element_Submit('submit', array(
            'label' => 'Сменить статус',
            'class' => 'btn btn-primary',
        ));

        $submit->setDecorators(array('ViewHelper'));

        $this->addElement($submit);

        // Группа полей кнопок
        $this->addDisplayGroup(
            array('status'), 'statusGroup', array('legend' => 'Статус')
        );

        // Группа полей кнопок
        $this->addDisplayGroup(
            array('submit'), 'buttonsGroup', array('legend' => '')
        );

    }


}

