<?php

class Application_Form_Register extends Lib_Form {

    /**
     * Инициализация 
     */
    public function init() {
        parent::init();

        // Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction($helperUrl->url(array(), 'register'));

        // Метод формы
        $this->setMethod('post');

        // Атрибут class для формы
        $this->setAttrib('class', 'form form-horizontal');

        // Используемый собственный элемент
        $email = new Lib_Form_Element_Email('email', array(
                    'required' => true,
                ));

        $this->addElement($email);

        // Login 
        $login = new Zend_Form_Element_Text('login', array(
                    'required' => true,
                    'label' => 'Выберите логин',
                    'maxlength' => '32',
                    'validators' => array(
                        array('Alnum', true, array(true)),
                        array('StringLength', true, array(0, 32))),
                    'filters' => array('StringTrim'),
                ));

        //TODO запрет регистрации логина как у агента
        $dbValidator = $validator = new Zend_Validate_Db_NoRecordExists(array(
                    'table' => 'users',
                    'field' => 'login'
                ));
        $dbValidator->setMessages(array(Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => '%value% логин уже зарегистрирован'));
        $login->addValidator($dbValidator);
        $this->addElement($login);


        // Pwd элемент пароль. Значение проверяется собственным валидатором пароля
        $password = new Zend_Form_Element_Password('password', array(
                    'required' => true,
                    'label' => 'Пароль',
                    'maxlength' => '15',
                    'validators' => array('Password'),
                    'value' => '',
                ));
        $this->addElement($password);


        // Подтверждение пароля
        $passwordApprove = new Zend_Form_Element_Password('password_approve', array(
                    'required' => true,
                    'label' => 'Подтвердите пароль',
                    'maxlength' => '15',
                    'ignore' => true,
                    'validators' => array(array('EqualInputs', true, array('password'))),
                ));
        $this->addElement($passwordApprove);

        // FIRSTNAME
        $firstname = new Zend_Form_Element_Text('firstname', array(
            'required' => false,
            'label' => 'Имя',
            'maxlength' => '32',
            'validators' => array(
                array('Alnum', true, array(true)),
                array('StringLength', true, array(0, 32))),
            'filters' => array('StringTrim'),
        ));
        $this->addElement($firstname);

        // LASTTNAME
        $lastname = new Zend_Form_Element_Text('lastname', array(
            'required' => false,
            'label' => 'Фамилия',
            'maxlength' => '32',
            'validators' => array(
                array('Alnum', true, array(true)),
                array('StringLength', true, array(0, 32))),
            'filters' => array('StringTrim'),
        ));
        $this->addElement($lastname);


        // INFO
        $info = new Zend_Form_Element_Textarea('info', array(
            'required' => false,
            'label' => 'Дополнительная информация',
            'cols' => '50',
            'rows' => '15',
            'maxlength' => '800',
            'filters' => array('HtmlEntities'),
        ));
        $this->addElement($info);



        // Организация
        $org = new Zend_Form_Element_Multiselect('oids');
        $org->setLabel('Организация')
            ->setRequired(false);
        //TODO автоматическое выставление верной организации
        $table = new Application_Model_OrganizationMapper();
        foreach ($table->fetchAll() as $organization) {
            $org->addMultiOptions(array($organization->oid => $organization->oid.':'.$organization->name));
        }
        $this->addElement($org);


        // telefon
        $telefon = new Zend_Form_Element_Text('telefon', array(
            'required' => false,
            'label' => 'Телефон',
            'maxlength' => '15',
            'validators' => array(
                array('Alnum', true, array(true)), //TODO валидатор телефонного номера
                array('StringLength', true, array(0, 15))),
            'filters' => array('StringTrim'),
        ));
        $this->addElement($telefon);

        // Кнопка Submit
        $submit = new Zend_Form_Element_Submit('submit', array(
                    'label' => 'Зарегистрировать',
                    'class' => 'btn btn-primary',
                ));

        $submit->setDecorators(array('ViewHelper'));

        $this->addElement($submit);


        // Группируем элементы
        // Группа полей связанных с авторизационными данными
        $this->addDisplayGroup(
                array('email', 'login', 'password', 'password_approve'), 'authDataGroup', array(
            'legend' => 'Авторизационные данные'
                )
        );

        // Группа полей связанных с личной информацией
        $this->addDisplayGroup(
                array('firstname', 'lastname', 'info', 'telefon'), 'privateDataGroup', array(
            'legend' => 'Личная информация'
                )
        );

        // Общая информация
        $this->addDisplayGroup(
                array('oids'), 'orgGroup', array(
            'legend' => 'Информация об организации'
                )
        );


        // Группа полей кнопок
        $this->addDisplayGroup(
                array('submit'), 'buttonsGroup', array('legend' => '')
        );
    }

}

