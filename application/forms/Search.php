<?php

class Application_Form_Search extends Lib_Form
{

    public function init()
    {
        // Указываем action формы
        $helperUrl = new Zend_View_Helper_Url();
        $this->setAction($helperUrl->url(array('controller' => 'search', 'action' => 'index'), 'default'));

        // Метод формы
        $this->setMethod('post');

        // Атрибут class для формы
        $this->setAttrib('class', 'form-horizontal');


        // Login 
        $login = new Zend_Form_Element_Text('login', array(
                    'required' => false,
                    'label' => 'Искать по логину: ',
                    'maxlength' => '15',
                    'validators' => array(
                        array('Alnum', true, array(true)),
                        array('StringLength', true, array(0, 50))),
                ));
        $this->addElement($login);
        
        // firstname 
        $firstname = new Zend_Form_Element_Text('firstname', array(
                    'required' => false,
                    'label' => 'Искать по имени: ',
                    'maxlength' => '15',
                    'validators' => array(
                        array('Alnum', true, array(true)),
                        array('StringLength', true, array(0, 50))),
                ));
        $this->addElement($firstname);
        
        
        // lastname 
        $lastname = new Zend_Form_Element_Text('lastname', array(
                    'required' => false,
                    'label' => 'Искать по фамилии: ',
                    'maxlength' => '15',
                    'validators' => array(
                        array('Alnum', true, array(true)),
                        array('StringLength', true, array(0, 50))),
                ));
        $this->addElement($lastname);
        
        // Возраст от
        $agefrom = new Zend_Form_Element_Text('agefrom', array(
                    'required' => false,
                    'label' => 'Возраст от: ',
                    'maxlength' => '15',
                    'validators' => array(
                        array('Digits', true),
                        array('Between', true, array('min' => 0, 'max' => 200)),
                )));
        $this->addElement($agefrom);
        // Возраст до 
        $ageto = new Zend_Form_Element_Text('ageto', array(
                    'required' => false,
                    'label' => 'Возраст до: ',
                    'maxlength' => '15',
                    'validators' => array(
                        array('Digits', true),
                        array('Between', true, array('min' => 0, 'max' => 200)),
                )));
        $this->addElement($ageto);


        // Пол
        $gender = new Zend_Form_Element_Radio('gender', array(
                    'label' => 'Пол:',
                    'multiOptions' => array('m' => 'Муж', 'f' => 'Жен'),
                        )
        );
        $gender->setSeparator(' ');
        $validator = new Zend_Validate_InArray(
                        array(
                            'haystack' => array('m', 'f'),
                            'strict' => true
                        )
        );

        $gender->addValidator($validator, true);
        $this->addElement($gender);


        // Кнопка Submit
        $submit = new Zend_Form_Element_Submit('submit', array(
                    'label' => 'Поиск',
                    'class' => 'btn-primary',
                ));

        $submit->setDecorators(array('ViewHelper'));

        $this->addElement($submit);

        // Кнопка Reset, возвращает форму в начальное состояние
        $reset = new Zend_Form_Element_Reset('reset', array(
                    'label' => 'Очистить',
                ));

        // Перезаписываем декораторы, что-бы выставить две кнопки в ряд
        $reset->setDecorators(array('ViewHelper'));
        $this->addElement($reset);


    }


}

