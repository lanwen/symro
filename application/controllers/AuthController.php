<?php

/**
 * Контроллер авторизации. Регистрация, авторизация и выход пользователя из системы.
 */
class AuthController extends Zend_Controller_Action {


    /**
     * Редиректор
     * @var Zend_Controller_Action_Helper_Redirector
     */
    protected $_redirector = null;

    /**
     * Инициализация редиректора
     */
    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function indexAction() {
        $this->_forward('login');
    }

    /**
     * Действие регистрации.
     * Выводит форму регистрации пользователя и обрабатывает ее данные.
     */
    public function registerAction() {
        $registrator = new Application_Model_UsersMapper();
        // Инициализируем форму регистрации
        $formRegister = new Application_Form_Register();
        // Проверяем тип запроса, если POST значит пришли данные формы
        if ($this->_request->isPost()) {


            // Проверяем на валидность поля формы
            if ($formRegister->isValid($this->_getAllParams())) {

                // Инициализируем объект отвечающий за таблицу пользователей
                $usr = new Application_Model_Users($formRegister->getValues());
                

                // Вставляем данные в базу данных
                $insertid = $registrator->save($usr);


                // Задаем сообщение о успешной регистрации
                 $this->_helper->FlashMessenger->setNamespace('messages')
                        ->addMessage('Новый пользователь добавлен!');

                // Перенаправление на страницу созданного юзера
                $this->_helper->redirector->gotoRoute(array('uid' => $insertid), 'user');
            }
        }
        // Передаем форму в скрипт вида
        $this->view->formRegister = $formRegister;
    }

    /**
     * Действие авторизации.
     * Сравнивает введенные данные с наличием похожих записей в БД
     */
    public function loginAction() {

        // Если уже авторизован, перенаправляем на главную
        $auth = Zend_Auth::getInstance();
        if($auth->hasIdentity()) {
            $this->_redirect('/');
        }
        $formLogin = new Application_Form_Login();
        // Если пришли данные
        if ($this->_request->isPost()) {
            if ($formLogin->isValid($this->_request->getPost())) {
                $authAdapter = $this->initAuthAdapter();
                $authAdapter->setIdentity($formLogin->getValue('login'))
                        ->setCredential(md5($formLogin->getValue('password')));
                $result = $auth->authenticate($authAdapter); // Проверяем есть ли такая запись в бд
                if ($result->isValid()) { // Если есть
                     $AuthStorage = $auth->getStorage();
                     $AuthStorage->write($authAdapter->getResultRowObject());
                     $this->_helper->FlashMessenger->setNamespace('messages')
                             ->addMessage('Вы успешно залогинились!');

                     $this->_redirect('/');
                 } else { // Если записи нет
                     $this->_helper->FlashMessenger->setNamespace('errors')
                             ->addMessage('Ошибка в логине или пароле, попробуйте еще раз');
                     $this->_redirect('/login');
                 }
                
            }
        }
        // Передача данных в скрипт вида
        $this->view->loginform = $formLogin;
    }

    /**
     * Уничтожение сессии, выход из системы
     */
    public function logoutAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        $this->_redirect('auth/login');
    }

    /**
     * Подготавливает к использованию аутенфикатор-адаптер
     * @return \Zend_Auth_Adapter_DbTable 
     */
    private function initAuthAdapter() {
        $authAdapter = new Zend_Auth_Adapter_DbTable();
        $authAdapter->setTableName('users')
                ->setIdentityColumn('login')
                ->setCredentialColumn('password');
        return $authAdapter;
    }

}

