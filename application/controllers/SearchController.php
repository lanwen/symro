<?php

class SearchController extends Zend_Controller_Action {

    public function init() {
        $serviceOptions = $this->getInvokeArg('bootstrap')->getOption('images');
        $this->view->imgopt = $serviceOptions;
        $this->view->gender = array('f' => 'Пани', 'm' => 'Пан');
    }

    public function indexAction() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/');
        }

        // Инициализируем форму регистрации
        $form = new Application_Form_Search();
        // Проверяем тип запроса, если POST значит пришли данные формы
        //print_r($formRegister);
        if ($this->_request->isPost()) {


            // Проверяем на валидность поля формы
            if ($form->isValid($this->_getAllParams())) {

                // Инициализируем объект отвечающий за таблицу пользователей
                $searcher = new Application_Model_Search();
                $users = $searcher->search($form->getValues());
                $this->view->itwassearch = true;
                $this->view->users = $users;

                //TODO Пагинатор
            }
        } else {
            
            $this->view->itwassearch = false;
        }
        // Передаем форму в скрипт вида
        $this->view->form = $form;
    }

}

