<?php

/**
 * Контроллер компьютеров. Создает, удаляет, выводит компьютеры
 */
class CompController extends Zend_Controller_Action
{

    /**
     * Редиректор
     * @var Zend_Controller_Action_Helper_Redirector
     */
    protected $_redirector = null;
    protected $_acl = null;

    /**
     * Инициализация полей
     */
    public function init()
    {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        // Для проверки контроля доступа в скриптах вида
        $this->_acl = new Lib_Acl_Acl();
        $this->view->acl = $this->_acl;
    }

    /**
     * Действие по-умолчанию
     * Вывод списка доступных компьютеров
     */
    public function indexAction()
    {
        $compMapper = new Application_Model_ComputersMapper();
        if ($this->_acl->isAllowed(Zend_Auth::getInstance()->getIdentity()->role, 'mvc:comp', 'viewall')) {
            $comps = $compMapper->fetchAll();
        } else {
            $comps = $compMapper->fetchAll(Zend_Auth::getInstance()->getIdentity()->uid);
        }
        $this->view->comps = $comps;
    }

    /**
     * Действие просмотра компьютера
     * Выводит подробную информацию о компьютере
     */
    public function viewAction()
    {
        $cid = $this->_request->getParam('cid', FALSE);
        if (!$cid || 0 == (int)$cid) {
            $this->_redirector->gotoSimple('index', 'comp');
        }

        $compMapper = new Application_Model_ComputersMapper();

        if(!$compMapper->isCompOfUser($cid, Zend_Auth::getInstance()->getIdentity()->uid)
        && !$this->_acl->isAllowed(Zend_Auth::getInstance()->getIdentity()->role, 'mvc:comp', 'viewall')) {
            $this->_redirector->gotoSimple('index', 'comp');
        }
        $this->view->comp = $compMapper->find($cid);

        $usersMapper = new Application_Model_UsersMapper();
        $users = $usersMapper->getUsersByCid($cid);
        $this->view->users = $users;
    }

    /**
     * Редактирование компьютера
     * В случае, если параметром не передан номер компьютера - создает новый
     * Выводит форму в скрипт вида
     */
    public function editAction()
    {
        $editor = new Application_Model_ComputersMapper();
        // Инициализируем форму регистрации
        $formEdit = new Application_Form_Comp();
        // Проверяем тип запроса, если POST значит пришли данные формы

        if ($this->_request->isPost()) {
            // Проверяем на валидность поля формы
            if ($formEdit->isValid($this->_getAllParams())) {
                // Причесываем данные
                $formValues = $formEdit->getValues();
                $formValues['oid'] = $formValues['orgName'];
                unset($formValues['orgName']);

                $comp = new Application_Model_Computers($formValues);

                // Сохранение модели
                $id = $editor->save($comp);

                //DEBUG ----------------
                //Zend_Debug::dump($id);
                //----------------------

                $comp->setCid($id); // Назначение модели уникального номера для дальнейшего отображения
                // Задаем сообщение о успешной операции
                $this->_helper->FlashMessenger->setNamespace('messages')->addMessage('Данные обновлены!');
                // Перенаправление на главную страницу
                $this->_redirector->gotoSimple('view', 'comp', 'default', array('cid' => $comp->getCid()));
            }
        } else { // Если параметров не было отправлено, выводим форму
            $comp = $editor->find($this->_request->getParam('cid', FALSE));
            if (!is_null($comp)) { // Если компьютер существует, заполняем форму существующими данными
                $formEdit->populate($comp->toArray());
            }
        }
        // Передаем форму в скрипт вида
        $this->view->form = $formEdit;
    }

    /**
     * Удаление компьютера
     */
    public function delAction()
    {
        $cid = $this->_request->getParam('cid', FALSE);
        if ($cid) {
            $editor = new Application_Model_ComputersMapper();
            $editor->delete((int)$cid);
        }
        $this->_redirector->gotoSimple('index', 'comp');

    }

}

