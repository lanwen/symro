<?php

class MsgController extends Zend_Controller_Action
{

    /**
     * Редиректор - определен для полноты кода
     *
     * @var Zend_Controller_Action_Helper_Redirector
     *
     *
     */
    protected $_redirector = null;
    protected $_acl = null;

    public function init()
    {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        // Для проверки контроля доступа в стилях
        $this->_acl = new Lib_Acl_Acl();
        $this->view->acl = $this->_acl;
    }

    public function indexAction()
    {
        $this->_redirector->gotoSimple('outbox', 'msg');
    }

    public function sendAction()
    {
        $suid = Zend_Auth::getInstance()->getIdentity()->uid;
        $ruid = $this->_request->getParam('ruid');
        $sender = new Application_Model_MsgMapper();
        // Инициализируем форму
        $form = new Application_Form_Msgsend(array('ruid' => $ruid));
        $userMapper = new Application_Model_UsersMapper();
        $user = $userMapper->find($ruid);

        if (!$user) {
            $this->_redirector->gotoSimple('index', 'users');
        }

        if ('admin' != $user->role &&
            !$this->_acl->isAllowed(Zend_Auth::getInstance()->getIdentity()->role, 'mvc:msg', 'sendnotadmins')) {
            $this->_redirector->gotoSimple('index', 'users');
        }

        // Проверяем тип запроса, если POST значит пришли данные формы
        if ($this->_request->isPost()) {
            $form->addElement($this->hiddenInputRuid($ruid));
            // Проверяем на валидность поля формы
            if ($form->isValid($this->_getAllParams())) {
                $msg = new Application_Model_Msg($form->getValues());
                $msg->setSuid($suid);
                // Вставляем данные в базу данных
                $sender->save($msg);
                // Задаем сообщение о успешной операции
                $this->_helper->FlashMessenger->setNamespace('messages')->addMessage('Сообщение отправлено!');
                // Перенаправление на страницу юзера-адресата
                $this->_redirector->gotoSimple('outbox', 'msg');
            }
        }

        $this->view->user = $user;

        // Передаем форму в скрипт вида
        $this->view->form = $form;
    }

    public function readAction()
    {
        $mid = $this->_request->getParam('mid');
        $uid = Zend_Auth::getInstance()->getIdentity()->uid;
        $msgesOperator = new Application_Model_MsgMapper();
        $msg = $msgesOperator->find($mid);
        if (Application_Model_Msg::STATUS_NEW == $msg->status && $uid == $msg->ruid) {
            $msg->setStatus(Application_Model_Msg::STATUS_OPEN);
            $msgesOperator->save($msg);
        }
        $this->view->msg = $msg;
    }

    public function inboxAction()
    {
        $auth = Zend_Auth::getInstance();
        $ruid = $auth->getIdentity()->uid;

        $statusGet = $this->_request->getParam('status', 0);
        $stArrays = Application_Model_Msg::arrayOfStatuses();
        $status = $stArrays[$statusGet];

        $msgesOperator = new Application_Model_MsgMapper();
        $msgSet = $msgesOperator->getMsgsByResipient($ruid, $status);
        $this->view->statusGet = $statusGet;
        $this->view->msgSet = $msgSet;
    }

    public function outboxAction()
    {
        $auth = Zend_Auth::getInstance();
        $suid = $auth->getIdentity()->uid;
        $msgesOperator = new Application_Model_MsgMapper();
        $msgSet = $msgesOperator->getMsgsBySender($suid);
        $this->view->msgSet = $msgSet;
    }

    /**
     * Добавляет скрытое поле ruid с проверкой
     * существования юзера
     * @param type $ruid - юид получателя
     * @return \Zend_Form_Element_Hidden
     *
     */
    private function hiddenInputRuid($ruid)
    {

        $hidden = new Zend_Form_Element_Hidden('ruid', array(
            'value' => $ruid,
        ));
        $dbValidator = $validator = new Zend_Validate_Db_RecordExists(array(
            'table' => 'users',
            'field' => 'uid'
        ));
        $dbValidator->setMessages(array(Zend_Validate_Db_RecordExists::ERROR_NO_RECORD_FOUND => 'Пользователя под номером %value% того...нету'));
        $hidden->addValidator($dbValidator);

        return $hidden;
    }

    public function changestatusAction()
    {
        $mid = $this->_request->getParam('mid');
        $msgMapper = new Application_Model_MsgMapper();
        $msg = $msgMapper->find($mid);

        $uid = Zend_Auth::getInstance()->getIdentity()->uid;
        if (is_null($msg) || $uid != $msg->ruid) { // Если заявка не этому админу
            $this->_redirector->gotoSimple('inbox', 'msg');
        }

        // Инициализируем форму
        $form = new Application_Form_Statuschange(array('mid' => $mid));

        // Проверяем тип запроса, если POST значит пришли данные формы
        if ($this->_request->isPost()) {
            // Проверяем на валидность поля формы
            if ($form->isValid($this->_getAllParams())) {
                $msg->setStatus(Application_Model_Msg::convertIntToStatus((int)$form->getValue('status')));
                Zend_Debug::dump($msg);
                // Вставляем данные в базу данных
                $msgMapper->save($msg);
                // Задаем сообщение о успешной операции
                $this->_helper->FlashMessenger->setNamespace('messages')->addMessage('Статус изменен!');
                // Перенаправление на страницу юзера-адресата
                $this->_redirector->gotoSimple('inbox', 'msg');
            }
        }

        // Передаем форму в скрипт вида
        $this->view->form = $form;
    }


}



