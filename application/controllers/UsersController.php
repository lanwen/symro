<?php

class UsersController extends Zend_Controller_Action
{

    /**
     * Редиректор - определен для полноты кода
     *
     * @var Zend_Controller_Action_Helper_Redirector
     *
     *
     *
     */
    protected $_redirector = null;

    public function init()
    {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->view->acl = new Lib_Acl_Acl();
    }

    public function indexAction()
    {
        $userMapper = new Application_Model_UsersMapper();
        $users = $userMapper->fetchAll();
        $this->view->users = $users;
    }

    /**
     * Просмотр своего профиля по умолчанию, либо
     * при заданном uid
     * просмотр пользователя с таким uid (только для
     * администратора)
     *
     *
     *
     */
    public function profileAction()
    {
        $uid = $this->_request->getParam('uid', FALSE);
        if (!$uid) {
            $this->_redirector->gotoSimple('index', 'users', 'default');
        }
        $usersMapper = new Application_Model_UsersMapper();
        $user = $usersMapper->find($uid);

        //TODO Переделать в ACL
        if (!$user
            || ('admin' != Zend_Auth::getInstance()->getIdentity()->role
                && 'superadmin' != Zend_Auth::getInstance()->getIdentity()->role
                && $user->uid != Zend_Auth::getInstance()->getIdentity()->uid)
                && ('admin' != $user->role && 'superadmin' != $user->role)
        ) {

            $this->_redirector->gotoRoute(array('uid' => Zend_Auth::getInstance()->getIdentity()->uid), 'user');
        }

        $this->view->user = $user;
    }

    /**
     * Редактирование профиля
     */
    public function editprofileAction()
    {
        $uid = $this->_request->getParam('uid', Zend_Auth::getInstance()->getIdentity()->uid);
        if (!$uid) {
            $this->_redirector->gotoSimple('index', 'users');
        }

        // Инициализируем форму регистрации
        $formEdit = new Application_Form_Editprofile(array('uid' => (int)$uid));

        $this->showFormOrEditProfile($uid, $formEdit);
    }

    /**
     * Редактирование СВОЕГО профиля
     */
    public function editselfAction()
    {
        $uid = Zend_Auth::getInstance()->getIdentity()->uid;

        // Инициализируем форму регистрации
        $formEdit = new Application_Form_Editprofile();
        $helperUrl = new Zend_View_Helper_Url();
        $formEdit->setAction($helperUrl->url(array(
            'controller' => 'users',
            'action' => 'editself'), 'default'));

        $this->showFormOrEditProfile($uid, $formEdit);
    }

    /**
     * Вспомогательная функция для вывода формы редактирования профиля
     * @param $uid
     * @param Application_Form_Editprofile $formEdit
     */
    public function showFormOrEditProfile($uid, Application_Form_Editprofile $formEdit)
    {
        $usersMapper = new Application_Model_UsersMapper();
        $user = $usersMapper->find($uid);
        if (!$user) {
            $this->_redirector->gotoSimple('index', 'users');
        }


        // Проверяем тип запроса, если POST значит пришли данные формы

        if ($this->_request->isPost()) {
            // Проверяем на валидность поля формы
            if ($formEdit->editform->isValid($this->_getAllParams())
                && $formEdit->emailchangeform->isValid($this->_getAllParams())
                && $formEdit->pwdchangeform->isValid($this->_getAllParams())
                && $formEdit->rolechangeform->isValid($this->_getAllParams())
            ) {

                $user->setEmail($formEdit->emailchangeform->getValue('email'));
                $user->setRole($formEdit->rolechangeform->getValue('role'));
                $user->setOptions($formEdit->editform->getValues());

                $pwd = $formEdit->pwdchangeform->getValue('password');
                if ('' != $pwd && !is_null($pwd)) {
                    $user->setPassword($pwd);
                }

                // DEBUG ////////////////////////////
                //Zend_Debug::dump($user);
                //Zend_Debug::dump($this->_getAllParams());
                //////////////////////////////////////

                $usersMapper->save($user);
                // Задаем сообщение о успешной операции
                $this->_helper->FlashMessenger->setNamespace('messages')->addMessage('Данные обновлены!');
                // Перенаправление на главную страницу
                $this->_redirector->gotoRoute(array('uid' => $uid), 'user');
            } else {
                $formEdit->populate($user->toArray());
            }
        } else {

            $formEdit->populate($user->toArray());
        }
        // Передаем форму в скрипт вида
        $this->view->form = $formEdit;
    }

    public function viewAction()
    {
        // action body
    }

    public function delorgAction()
    {
        $uid = $this->_request->getParam('uid', FALSE);
        $oid = $this->_request->getParam('oid', FALSE);
        if (!($uid | $oid)) {
            $this->_redirector->gotoSimple('index', 'users', 'default');
        }
        $usersMapper = new Application_Model_UsersMapper();
        $usersMapper->deleteUserorg($uid, $oid);
        $this->_redirector->gotoRoute(array('uid' => $uid), 'user');
    }

    public function addorgAction()
    {
        $userMapper = new Application_Model_UsersMapper();
        // Инициализируем форму регистрации
        $uid = $this->_request->getParam('uid', FALSE);
        if (!($uid)) {
            $this->_redirector->gotoSimple('index', 'users', 'default');
        }
        $formAddorg = new Application_Form_AddOrg(array('uid' => $uid));
        // Проверяем тип запроса, если POST значит пришли данные формы
        if ($this->_request->isPost()) {
            // Проверяем на валидность поля формы
            if ($formAddorg->isValid($this->_getAllParams())) {

                // Инициализируем объект отвечающий за таблицу пользователей
                $usr = $userMapper->find($uid);
                if ($usr) {
                    foreach ($formAddorg->getValue('oids') as $oid) {
                        $userMapper->addUserorg($uid, $oid);
                    }
                    // Задаем сообщение о успешной регистрации
                    $this->_helper->FlashMessenger->setNamespace('messages')
                        ->addMessage('Организация прикреплена!');
                }


                // Перенаправление на страницу юзера
                $this->_helper->redirector->gotoRoute(array('uid' => $uid), 'user');
            }
        }
        // Передаем форму в скрипт вида
        $this->view->form = $formAddorg;
    }


    public function delcompAction()
    {
        $uid = $this->_request->getParam('uid', FALSE);
        $oid = $this->_request->getParam('cid', FALSE);
        if (!($uid | $oid)) {
            $this->_redirector->gotoSimple('index', 'users', 'default');
        }
        $usersMapper = new Application_Model_UsersMapper();
        $usersMapper->deleteUsercomp($uid, $oid);
        $this->_redirector->gotoRoute(array('uid' => $uid), 'user');
    }

    public function addcompAction()
    {
        $userMapper = new Application_Model_UsersMapper();
        // Инициализируем форму регистрации
        $uid = $this->_request->getParam('uid', FALSE);
        $usr = $userMapper->find($uid);
        if (is_null($usr)) {
            $this->_redirector->gotoSimple('index', 'users', 'default');
        }
        $formAddcomp = new Application_Form_Usercomp(array('uid' => $uid,
            'oids' => $usr->oids,
            'cids' => $usr->cids,
        ));
        // Проверяем тип запроса, если POST значит пришли данные формы
        if ($this->_request->isPost()) {
            // Проверяем на валидность поля формы
            if ($formAddcomp->isValid($this->_getAllParams())) {

                // Инициализируем объект отвечающий за таблицу пользователей

                if ($usr) {
                    $userMapper->deleteUsercomp($uid);
                    foreach ($formAddcomp->getValue('cids') as $oid) {
                        $userMapper->addUsercomp($uid, $oid);
                    }


                    // Задаем сообщение о успешной регистрации
                    $this->_helper->FlashMessenger->setNamespace('messages')
                        ->addMessage('Компьютер прикреплен!');
                }

                // Перенаправление на страницу юзера
                $this->_helper->redirector->gotoRoute(array('uid' => $uid), 'user');
            }
        }
        // Передаем форму в скрипт вида
        $this->view->form = $formAddcomp;
    }


}



