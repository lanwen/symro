<?php

class OrgController extends Zend_Controller_Action
{

    /**
     * Редиректор - определен для полноты кода
     *
     * @var Zend_Controller_Action_Helper_Redirector
     *
     *
     *
     */
    protected $_redirector = null;
    protected $_acl = null;

    public function init()
    {
        $this->_redirector = $this->_helper->getHelper('Redirector');

        $this->_helper->contextSwitch()
            ->addContext('ini', array(
            'suffix' => 'ini',
            'headers' => array(
                'Content-Type' => 'text/ini',
                'Content-Disposition' => 'attachment; filename=config.ini'
            )))
            ->addActionContext('config', array('xml', 'ini'))
            ->addActionContext('agentcreate', 'json')
            ->initContext();
        // Для проверки контроля доступа в стилях
        $this->_acl = new Lib_Acl_Acl();
        $this->view->acl = $this->_acl;
    }

    public function indexAction()
    {
        $orgMapper = new Application_Model_OrganizationMapper();
        if ($this->_acl->isAllowed(Zend_Auth::getInstance()->getIdentity()->role, 'mvc:org', 'viewall')) {
            $orgs = $orgMapper->fetchAll();
            $this->view->orgs = $orgs;
        } else {
            $orgs = $orgMapper->fetchAll(Zend_Auth::getInstance()->getIdentity()->uid);
            $this->view->orgs = $orgs;
        }
    }

    public function viewAction()
    {
        $oid = $this->_request->getParam('oid', FALSE);
        if (!$oid) {
            $this->_redirector->gotoSimple('index', 'org');
        }

        $orgMapper = new Application_Model_OrganizationMapper();
        $this->view->org = $orgMapper->find($oid);

        if(!$orgMapper->isOrgOfUser($oid, Zend_Auth::getInstance()->getIdentity()->uid)) {
            $this->_redirector->gotoSimple('index', 'org');
        }

        $usersMapper = new Application_Model_UsersMapper();
        $agentLogin = $this->getAgentLogin($oid);
        $agent = $usersMapper->getUserByLogin($agentLogin);

        $this->view->isAgentInitialized = !is_null($agent);


        $compMapper = new Application_Model_ComputersMapper();
        $this->view->comps = $compMapper->fetchByOid($oid);


        $users = $usersMapper->getUsersByOid($oid);
        $this->view->users = $users;

    }

    public function editAction()
    {
        //$this->_helper->FlashMessenger->setNamespace('errors')->addMessage('Данные!');
        $editor = new Application_Model_OrganizationMapper();
        // Инициализируем форму регистрации
        $formEdit = new Application_Form_Org();

        // Проверяем тип запроса, если POST значит пришли данные формы

        if ($this->_request->isPost()) {
            // Если элемент новый, то обязуем заполнять префикс
            if ('' == $this->_getParam('oid')) {
                $formEdit->getElement('prefix')->setRequired(true);
            }

            // Проверяем на валидность поля формы
            if ($formEdit->isValid($this->_getAllParams())) {

                // Причесываем данные
                $formValues = $formEdit->getValues();
                if (is_null($formValues['prefix']) || '' == $formValues['prefix']) {
                    unset($formValues['prefix']);
                }

                $org = new Application_Model_Organization($formValues);

                $id = $editor->save($org);

                //DEBUG ----------------
                Zend_Debug::dump($id);
                Zend_Debug::dump($formValues);
                //----------------------

                $org->setOid($id);
                // Задаем сообщение о успешной операции
                $this->_helper->FlashMessenger->setNamespace('messages')->addMessage('Данные обновлены!');
                // Перенаправление на главную страницу
                $this->_redirector->gotoSimple('view', 'org', 'default', array('oid' => $org->getOid()));
            }
        } else {
            $org = $editor->find($this->_request->getParam('oid', FALSE));
            if (!is_null($org) && $org) {
                $formEdit->removeElement('prefix');
                $formEdit->populate($org->toArray());
            }
        }
        // Передаем форму в скрипт вида
        $this->view->form = $formEdit;
    }

    public function delAction()
    {
        $oid = $this->_request->getParam('oid', FALSE);
        if ($oid) {
            $editor = new Application_Model_OrganizationMapper();
            $editor->delete((int)$oid);
        }
        $this->_redirector->gotoSimple('index', 'org');

    }

    public function configAction()
    {
        Zend_Controller_Front::getInstance()->unregisterPlugin('ZFDebug_Controller_Plugin_Debug');

        $oid = $this->_request->getParam('oid', FALSE);
        if (!$oid) {
            $this->_redirector->gotoSimple('index', 'org');
        }

        $orgMapper = new Application_Model_OrganizationMapper();
        $org = $orgMapper->find((int)$oid);

        if (is_null($org)) {
            $this->_redirector->gotoSimple('index', 'org');
        }
        $agent = Zend_Registry::get('agent');
        $generatedHash = md5($org->oid . $org->prefix . $agent['salt']);

        $this->view->org = $org;

        $config = new Application_Model_Config();

        $config->setBaseurl($this->getBasePath())
            ->setKey($generatedHash)
            ->setOid($oid)
            ->setLogin($this->getAgentLogin($oid))
            ->setPwd($this->getAgentPassword($oid));
        $this->view->config = $config;

        $doc = new DOMDocument();
        $doc->formatOutput = true;
        $root = $doc->createElement('main');
        $doc->appendChild($root);
        foreach ($config->toArray() as $key => $value) {
            $root->appendChild($doc->createElement($key, $value));
        }
        $xml = $doc->saveXML();
        $this->view->xml = $xml;
    }

    /**
     * Создание нового агента для организации или вывод уже существующего
     */
    public function agentcreateAction()
    {
        Zend_Controller_Front::getInstance()->unregisterPlugin('ZFDebug_Controller_Plugin_Debug');
        $oid = $this->_request->getParam('oid', FALSE);
        if (!$oid) {
            $this->_redirector->gotoSimple('index', 'org');
        }

        $orgMapper = new Application_Model_OrganizationMapper();
        if (is_null($orgMapper->find($oid))) {
            $this->_redirector->gotoSimple('index', 'org');
        }

        $agentLogin = $this->getAgentLogin($oid);
        $agentPwd = $this->getAgentPassword($oid);

        $mapper = new Application_Model_UsersMapper();
        $agent = $mapper->getUserByLogin($agentLogin);
        if (is_null($agent)) {
            $agent = new Application_Model_Users();
            $agent->setLogin($agentLogin)
                ->setRole('agent')
            // ->setOid($oid)
                ->setPassword($agentPwd);
            $mapper->save($agent);

            // Задаем сообщение о создании нового агента
            $this->_helper->FlashMessenger->setNamespace('messages')
                ->addMessage('Новый агент добавлен!');
            $this->_redirector->gotoSimple('agentcreate', 'org', 'default', array('oid' => $oid));

        }

        $this->view->agent = $agent;
        $this->view->agentPwd = $agentPwd;

        //!!! Вот так выдавать жсон $this->getResponse()->appendBody($this->_helper->json($data,false));
    }


    /**
     * Генерирует логин для агента
     * @param $oid - номер организации (целое число)
     * @return string
     */
    private function getAgentLogin($oid)
    {
        return 'agentOrg' . (int)$oid;
    }

    /**
     * Возвращает пароль для агента. Пароль основывается на соли из конфига
     * @param $oid - номер организации (целое число)
     * @return string
     */
    private function getAgentPassword($oid)
    {
        $registryAgent = Zend_Registry::get('agent');
        return substr(md5($this->getAgentLogin($oid) . $registryAgent['salt']), 0, 10);
    }

    /**
     * Составляет из объекта запроса адрес сайта
     * @return string
     */
    private function getBasePath()
    {
        return $this->getRequest()->getScheme()
            . '://'
            . $this->getRequest()->getHttpHost()
            . $this->getRequest()->getBaseUrl()
            . '/';
    }

}





