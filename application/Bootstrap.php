<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * Инициализация маршрутов
     */
    protected function _initRoutes()
    {
        //Zend_Controller_Front::getInstance()->setParam('useDefaultControllerAlways', true);

        $router = Zend_Controller_Front::getInstance()->getRouter();
        // Avatar show
        /* $routeUser = new Zend_Controller_Router_Route('/:av', array(
          'controller' => 'index',
          'action' => 'index',
          'module' => 'default',
          'av' => '1'
          ),
          array('av' => '\d+'));
          $router->addRoute('avatars', $routeUser); */

        // User show
        $routeUser = new Zend_Controller_Router_Route('/user/:uid/', array(
                'controller' => 'users',
                'action' => 'profile',
                'module' => 'default',
            ),
            array('uid' => '\d+'));
        $router->addRoute('user', $routeUser);

        // Userorg add
        $routeAddorg = new Zend_Controller_Router_Route('/uoadd/:uid/', array(
                'controller' => 'users',
                'action' => 'addorg',
                'module' => 'default',
            ),
            array('uid' => '\d+'));
        $router->addRoute('uoadd', $routeAddorg);

        // Usercomp add
        $routeAddcomp = new Zend_Controller_Router_Route('/ucadd/:uid/', array(
                'controller' => 'users',
                'action' => 'addcomp',
                'module' => 'default',
            ),
            array('uid' => '\d+'));
        $router->addRoute('ucadd', $routeAddcomp);

        // Comp show
        $routeComp = new Zend_Controller_Router_Route('/comp/:cid/', array(
                'controller' => 'comp',
                'action' => 'view',
                'module' => 'default',
                //'cid' => '1'
            ),
            array('cid' => '\d+'));
        $router->addRoute('comp', $routeComp);


        // Org show
        $routeOrg = new Zend_Controller_Router_Route('/org/:oid/', array(
                'controller' => 'org',
                'action' => 'view',
                'module' => 'default',
                //'cid' => '1'
            ),
            array('oid' => '\d+'));
        $router->addRoute('org', $routeOrg);

        //Msgsend
        $routeMsg = new Zend_Controller_Router_Route('/sendmsg/:ruid', array(
                'controller' => 'msg',
                'action' => 'send',
                'module' => 'default',
            ),
            array('ruid' => '\d+'));
        $router->addRoute('sendmsg', $routeMsg);

        //Msgread
         $routeMsg = new Zend_Controller_Router_Route('/readmsg/:mid', array(
          'controller' => 'msg',
          'action' => 'read',
          'module' => 'default',
          ),
          array('mid' => '\d+'));
          $router->addRoute('readmsg', $routeMsg);


        // Login
        $routeLogin = new Zend_Controller_Router_Route_Static('login',
            array('controller' => 'auth', 'action' => 'login'));
        $router->addRoute('login', $routeLogin);

        // Logout
        $routeLogout = new Zend_Controller_Router_Route_Static('logout',
            array('controller' => 'auth', 'action' => 'logout'));
        $router->addRoute('logout', $routeLogout);

        // Register
        $routeReg = new Zend_Controller_Router_Route_Static('register',
            array('controller' => 'auth', 'action' => 'register'));
        $router->addRoute('register', $routeReg);
    }

    /**
     * Инициализация плагинов
     */
    protected function _initPlugins()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Lib_Controller_Plugin_FlashMessenger());
    }

    protected function _initPlaceholders()
    {

        $this->bootstrapView();


        $view = $this->getResource('View');
        $view->doctype('XHTML1_TRANSITIONAL');


        // Set the initial title and separator:

        $view->headTitle('SYMRO')
            ->setSeparator(' :: ');

        //css
        $view->headLink()->prependStylesheet('/symro/assets/css/bootstrap-responsive.css');
        $view->headLink()->prependStylesheet('/symro/assets/css/bootstrap.css');
        $view->headLink()->prependStylesheet('/symro/assets/css/smoothness/jquery-ui-1.8.18.custom.css');


        $view->headScript()->appendFile('/symro/assets/js/jquery-1.7.1.js');
        $view->headScript()->appendFile('/symro/assets/js/jquery-ui-1.8.18.custom.min.js');
        $view->headScript()->appendFile('http://jquery-ui.googlecode.com/svn/trunk/ui/i18n/jquery.ui.datepicker-ru.js');
        $view->headScript()->appendFile('/symro/assets/js/bootstrap.js');

        //meta
        $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=UTF-8');
        $view->headMeta()->appendHttpEquiv('viewport', 'width=device-width, initial-scale=1.0');
    }

    /**
     * Инициализация отладочной панели
     */
    protected function _initZFDebug()
    {

        $options = array(
            'plugins' => array(
                'Variables',
                //   'File' => array('base_path' => '/'),
                'Memory',
                'Time',
                'Registry',
                'Exception',
                'Html',
            )
        );

        if ($this->hasOption('zfdebug')) {
            // Create ZFDebug instance
            $zfdebug = new ZFDebug_Controller_Plugin_Debug($this->getOption('zfdebug'));
            if ($this->hasPluginResource('db')) {
                $this->bootstrap('db');
                $db = $this->getPluginResource('db')->getDbAdapter();
                $options['plugins']['Database']['adapter'] = $db;
                $zfdebug->registerPlugin(new ZFDebug_Controller_Plugin_Debug_Plugin_Database($options));
            }

            // Register ZFDebug with the front controller
            $frontController = Zend_Controller_Front::getInstance();
            $frontController->registerPlugin($zfdebug);
        }
    }

    /**
     * Инициализируем ACL.
     * Создаем роли и ресурсы. Раздаем права доступа
     *
     * @return Zend_Acl
     */
    protected function _initAcl()
    {
        $auth = Zend_Auth::getInstance();
        // Определяем роль пользователя.
        // Если не авторизирован - значит "гость"
        $role = ($auth->hasIdentity()) ? $auth->getIdentity()->role : 'guest';

        $acl = new Lib_Acl_Acl();

        // Цепляем ACL к Zend_Navigator
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole($role);
        // Регистрируем плагин доступа
        Zend_Controller_Front::getInstance()->registerPlugin(new Lib_Controller_Plugin_Acl($acl));
    }

    /*
     * Инициализируем объект навигатора и передаем его в View
     *
     * @return Zend_Navigation
     */

    public function _initNavigation()
    {
        $this->bootstrapView();
        $view = $this->getResource('view');
        $auth = Zend_Auth::getInstance();

        $pages = array(
            array(
                'controller' => 'index',
                'route' => 'default',
                'label' => 'Главная',
                //'class' => 'blah',
            ),
            /*array(
                'controller' => 'users',
                'action' => 'profile',
                'resource' => 'mvc:users',
                'privilege' => 'profile',
                'route' => 'default',
                'label' => 'Моя страница',
                'pages' => array(
                    array(
                        'controller' => 'users',
                        'action' => 'editprofile',
                        'route' => 'default',
                        'resource' => 'mvc:users',
                        'privilege' => 'editprofile',
                        'label' => 'Ред.',
                    ),
                )),*/
            array(
                'controller' => 'msg',
                'action' => 'outbox',
                'resource' => 'mvc:msg',
                'privilege' => 'outbox',
                'route' => 'default',
                'label' => 'Заявки',
                'pages' => array(
                    array(
                        'controller' => 'msg',
                        'action' => 'inbox',
                        'route' => 'default',
                        'resource' => 'mvc:msg',
                        'privilege' => 'inbox',
                        'label' => 'Входящие заявки',
                    ),
                )),
            array(
                'controller' => 'users',
                'action' => 'index',
                'route' => 'default',
                'resource' => 'mvc:users',
                'privilege' => 'index',
                'label' => 'Пользователи',
                'pages' => array(
                    array(
                        'route' => 'register',
                        'resource' => 'mvc:auth',
                        'privilege' => 'register',
                        'label' => 'Регистрация',
                    ),
                )),
            /*  array(
                'controller' => 'search',
                'action' => 'index',
                'route' => 'default',
                'resource' => 'mvc:search',
                'privilege' => 'search',
                'label' => 'Поиск',
            ),*/
            array(
                'controller' => 'org',
                'action' => 'index',
                'route' => 'default',
                'resource' => 'mvc:org',
                'privilege' => 'index',
                'label' => 'Организации',
                'pages' => array(
                    array(
                        'controller' => 'org',
                        'action' => 'edit',
                        'route' => 'default',
                        'resource' => 'mvc:org',
                        'privilege' => 'edit',
                        'label' => 'Добавить',
                    ),
                )),
            array(
                'controller' => 'comp',
                'action' => 'index',
                'route' => 'default',
                'resource' => 'mvc:comp',
                'privilege' => 'index',
                'label' => 'Компьютеры',
                'pages' => array(
                    array(
                        'controller' => 'comp',
                        'action' => 'edit',
                        'route' => 'default',
                        'resource' => 'mvc:comp',
                        'privilege' => 'edit',
                        'label' => 'Добавить',
                    ),
                )),
            array(
                'controller' => 'users',
                'action' => 'profile',
                'route' => 'default',
                'params' => array('uid' => (Zend_Auth::getInstance()->hasIdentity())
                    ? Zend_Auth::getInstance()->getIdentity()->uid
                    : ''),
                'resource' => 'mvc:users',
                'privilege' => 'profile',
                'label' => 'Свой профиль',
                'pages' => array(
                    array(
                        'controller' => 'users',
                        'action' => 'editself',
                        'route' => 'default',
                        'resource' => 'mvc:users',
                        'privilege' => 'editself',
                        'label' => 'Ред. профиль',
                    ),
                )),
            array(
                'route' => 'login',
                'resource' => 'mvc:auth',
                'privilege' => 'login',
                'label' => 'Вход',
            ),
            array(
                'route' => 'logout',
                'resource' => 'mvc:auth',
                'privilege' => 'logout',
                'label' => 'Выход [' . (($auth->hasIdentity()) ? $auth->getIdentity()->login : null) . ']',
            ),
        );

        $container = new Zend_Navigation($pages);

        $view->navigation($container);

        return $container;
    }

    public function _initRegistryVars()
    {
        Zend_Registry::set('agent', $this->getOption('agent'));

    }

}

